package com.micro.platform.oauth.framework.exception;

public enum OauthExceptionEnum {
    /**
     * 默认，处理错误
     */
    Default,
    /**
     * 登录请求body为空
     */
    LoginRequestBodyIsNull(901, "login request's body is null"),

    /**
     * 登录请求grantType为空
     */
    LoginRequestGrantTypeIsNull(902, "login request's grantType is null"),

    /**
     * 登录请求grantType未找到
     */
    LoginRequestGrantTypeNotFind(903, "login request's grantType is not find"),

    /**
     * accessToken 为空
     */
    AccessTokenIsNotEmpty(904, "accessToken is not Empty"),

    /**
     * ClientDetailsService不存在
     */
    ClientDetailsServiceIsNotExist(905, "ClientDetailsService is not exist"),

    /**
     * UserDetailsService不存在
     */
    UserDetailsServiceIsNotExist(906, "UserDetailsService is not exist"),

    /**
     * 授权码创建失败
     */
    AuthorizationCodeCreateFiled(907, "authorization code filed to Create"),

    /**
     * access token创建失败
     */
    AccessTokenCreateFiled(908, "access token filed to Create"),

    /**
     * refresh token创建失败
     */
    RefreshTokenCreateFiled(909, "refresh token filed to Create"),

    /**
     * clientId不存在
     */
    ClientIdIsNotExist(910, "client id is not exist"),

    /**
     * userId不存在
     */
    UserIdIsNotExist(911, "userId id is not exist"),

    /**
     * 授权码不能为空
     */
    AuthorizationCodeIsNotEmpty(912, "authorization code is not empty"),

    /**
     * 授权码校验失败
     */
    AuthorizationCodeValidateFiled(913, "authorization code validate filed"),

    /**
     * 用户账户不存在
     */
    UserAccountIsNotExist(914, "user account id is not exist"),

    /**
     * refreshToken 为空
     */
    RefreshTokenIsNotEmpty(915, "refreshToken is not Empty"),
    /**
     * accessToken 无效
     */
    RefreshTokenValidateFiled(916, "refreshToken validate filed"),
    /**
     * clientSecret 无效
     */
    ClientSecretValidateFiled(917, "clientSecret validate filed"),
    /**
     * captcha code or password 不正确
     */
    captchaCodeOrPasswordError(918,"captcha code or password validate filed"),
    ;
    private String message = "oauth error";
    private int errorCode = 900;
    OauthExceptionEnum(){}
    OauthExceptionEnum(int errorCode, String message){
        this.errorCode = errorCode;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
