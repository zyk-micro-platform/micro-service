package com.micro.platform.oauth.framework.exception;

import lombok.Data;

@Data
public class OauthException extends RuntimeException {
    protected int errorCode;

    public OauthException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }
    public OauthException(OauthExceptionEnum oauthExceptionEnum) {
        super(oauthExceptionEnum.getMessage());
        this.errorCode = oauthExceptionEnum.getErrorCode();
    }

    public OauthException(OauthExceptionEnum oauthExceptionEnum, String message) {
        super(message);
        this.errorCode = oauthExceptionEnum.getErrorCode();
    }

    public OauthException(String message, Throwable cause, int errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public OauthException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, int errorCode) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }
}
