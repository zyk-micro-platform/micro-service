package com.micro.platform.oauth.framework.interfaces;

import com.micro.platform.oauth.framework.domain.UserDetails;

public interface IUserDetailsService {
    UserDetails loadUserDetailsByAccount(String account);
}
