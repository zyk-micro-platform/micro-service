package com.micro.platform.oauth.controller.rsa;

import com.micro.platform.oauth.component.RsaComponent;
import com.micro.platform.starter.domain.Result;
import com.micro.platform.starter.utils.RSACoderUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


@Api(tags = "rsa信息")
@RestController
@RequestMapping("/rsa")
public class RSAController {

    @Autowired
    private RsaComponent rsaComponent;

    @ApiOperation("创建RSA公密钥")
    @GetMapping("/created")
    public Result<RsaResponse> created(){
        RsaResponse rsaResponse = new RsaResponse();
        try {
            RSACoderUtil.KeyPairBuilder keyPairBuilder = RSACoderUtil.createdKeyPairBuilder();
            String publicKey = keyPairBuilder.publicKey();
            String privateKey = keyPairBuilder.privateKey();
            String rsaKeyId = UUID.randomUUID().toString();
            rsaResponse.setPublicKey(publicKey);
            rsaResponse.setRsaKeyId(rsaKeyId);
            rsaComponent.savePrivateKey(rsaKeyId, privateKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.ok(rsaResponse);
    }
}
