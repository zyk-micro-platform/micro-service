package com.micro.platform.oauth.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * oauth-client表
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OauthClient extends Model<OauthClient> {

    private static final long serialVersionUID = 1L;

    /**
     * 客户端ID
     */
    @TableId(value = "client_id")
    private String clientId;

    /**
     * 客户端密钥
     */
    private String clientSecret;

    /**
     * 客户端名称
     */
    private String clientName;

    /**
     * 权限
     */
    private String authorities;

    /**
     * 认证类型
     */
    private String authorizedGrantTypes;

    /**
     * 静默登录
     */
    private String autoApprove;

    /**
     * 过期时间
     */
    private LocalDateTime expirationDate;

    /**
     * access_token有效时间
     */
    private Integer accessTokenValidity;

    /**
     * refresh_token有效时间
     */
    private Integer refreshTokenValidity;

    /**
     * 作用域
     */
    private String scope;

    /**
     * 重定向地址
     */
    private String redirectUri;

    /**
     * 是否可用 1-可用  0-不可用
     */
    private Boolean enable;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 密钥类型: 0 动态绑定, 1 静态生成，使用publicKey字段 进行解密
     */
    private Integer secretKeyType;

    /**
     * 公钥
     */
    private String publicKey;

    /**
     * 私钥
     */
    private String privateKey;

    @Override
    protected Serializable pkVal() {
        return this.clientId;
    }

}
