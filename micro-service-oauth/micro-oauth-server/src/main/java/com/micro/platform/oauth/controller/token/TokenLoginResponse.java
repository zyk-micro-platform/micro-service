package com.micro.platform.oauth.controller.token;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("登录响应信息")
public class TokenLoginResponse implements Serializable {
    private static final long serialVersionUID = -4219865251523650864L;
    @ApiModelProperty("accessToken")
    private String accessToken;
    @ApiModelProperty("超时时间 秒")
    private Long expiresTime;
    @ApiModelProperty("超时时间戳 毫秒")
    private Long expiresTimeStamp;
    @ApiModelProperty("refreshToken")
    private String refreshToken;
    @ApiModelProperty("refreshToken超时时间 秒")
    private Long refreshExpiresTime;
    @ApiModelProperty("账户")
    private String account;
    @ApiModelProperty("客户端ID")
    private String clientId;
    @ApiModelProperty("域")
    private String[] scopes;
    @ApiModelProperty("用户ID")
    private Long userId;
    @ApiModelProperty("用户昵称")
    private String nickName;
    @ApiModelProperty("电话")
    private String phone;
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty("注册时间")
    private String createTime;
    @ApiModelProperty("角色")
    private List<String> roles;
}
