package com.micro.platform.oauth.framework.interfaces;

import com.micro.platform.oauth.framework.constants.OauthConstants;
import com.micro.platform.oauth.framework.domain.AuthorizationDetails;
import com.micro.platform.oauth.framework.domain.OauthContext;

import java.util.Map;

public interface ITokenConvert {
    default AuthorizationDetails convert(OauthContext context){
        return null;
    }

    void convert(OauthContext context, Map<String, Object> convertData);

    boolean registerAccessTokenConvert(ITokenConvert tokenConvert);

    boolean registerRefreshTokenConvert(ITokenConvert tokenConvert);

    default Long getRefreshTokenValidityTime(OauthContext context) {
        Integer refreshTokenValidity = context.getClientDetails().getRefreshTokenValidity();
        if(refreshTokenValidity == null){
            refreshTokenValidity = OauthConstants.DEFAULT_REFRESH_EXPIRES_TIME;
        }
        return Long.valueOf(refreshTokenValidity);
    }

    default Long getAccessTokenValidityTime(OauthContext context) {
        Integer accessTokenValidity = context.getClientDetails().getAccessTokenValidity();
        if(accessTokenValidity == null){
            accessTokenValidity = OauthConstants.DEFAULT_ACCESS_EXPIRES_TIME;
        }
        return Long.valueOf(accessTokenValidity);
    }
}
