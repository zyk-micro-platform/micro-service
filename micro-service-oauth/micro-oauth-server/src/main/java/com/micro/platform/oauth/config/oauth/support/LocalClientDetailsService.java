package com.micro.platform.oauth.config.oauth.support;

import com.micro.platform.oauth.domain.LocalClientDetails;
import com.micro.platform.oauth.entity.OauthClient;
import com.micro.platform.oauth.framework.domain.ClientDetails;
import com.micro.platform.oauth.framework.interfaces.IClientDetailsService;
import com.micro.platform.oauth.service.OauthClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LocalClientDetailsService implements IClientDetailsService {

    @Autowired
    private OauthClientService oauthClientService;

    @Override
    public ClientDetails loadClientDetailsByClientId(String clientId) {
        OauthClient oauthClient = oauthClientService.getClientByClientId(clientId);
        LocalClientDetails localClientDetails = buildClientDetails(oauthClient);
        return localClientDetails;
    }

    private LocalClientDetails buildClientDetails(OauthClient oauthClient) {
        if(oauthClient == null){
            return null;
        }
        LocalClientDetails localClientDetails = new LocalClientDetails(oauthClient);
        return localClientDetails;
    }
}
