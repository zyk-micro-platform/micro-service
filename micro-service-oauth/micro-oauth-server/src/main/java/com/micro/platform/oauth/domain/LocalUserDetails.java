package com.micro.platform.oauth.domain;

import com.micro.platform.oauth.framework.domain.UserDetails;
import com.micro.platform.user.dto.UserApisDTO;

import java.time.LocalDateTime;
import java.util.List;


public class LocalUserDetails implements UserDetails {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String account;
    private Long deptId;
    private String name;
    private String phone;
    private String email;
    private String privateKey;
    private String password;
    private Boolean enable;
    private LocalDateTime createTime;
    private List<String> authorities;
    private List<String> apis;
    public LocalUserDetails(UserApisDTO userapisDTO){
        this.id = userapisDTO.getId();
        this.account = userapisDTO.getAccount();
        this.deptId = userapisDTO.getDeptId();
        this.name = userapisDTO.getName();
        this.phone = userapisDTO.getPhone();
        this.email = userapisDTO.getEmail();
        this.password = userapisDTO.getPassword();
        this.enable = userapisDTO.getEnable();
        this.createTime = userapisDTO.getCreateTime();
        this.authorities = userapisDTO.getRoleCodes();
        this.apis = userapisDTO.getApis();
    }

    @Override
    public List<String> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getAccount() {
        return this.account;
    }

    @Override
    public boolean isEnabled() {
        return this.enable == null ? false : this.enable;
    }

    public Long getId() {
        return id;
    }

    public Long getDeptId() {
        return deptId;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public List<String> getApis() {
        return apis;
    }
}
