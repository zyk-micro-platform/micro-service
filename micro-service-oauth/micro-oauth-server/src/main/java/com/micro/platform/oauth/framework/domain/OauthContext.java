package com.micro.platform.oauth.framework.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public class OauthContext implements Serializable {
    private static final long serialVersionUID = -5707507470608802206L;
    private UserDetails userDetails;
    private OauthRequest oauthRequest;
    private ClientDetails clientDetails;
    private String subjectKey;
    private final Map<String, Object> attributes = new HashMap<>();

    public OauthContext(UserDetails userDetails, OauthRequest oauthRequest, ClientDetails clientDetails) {
        this.userDetails = userDetails;
        this.oauthRequest = oauthRequest;
        this.clientDetails = clientDetails;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public OauthRequest getOauthRequest() {
        return oauthRequest;
    }

    public ClientDetails getClientDetails() {
        return clientDetails;
    }

    public String getSubjectKey() {
        return subjectKey;
    }

    public void setSubjectKey(String subjectKey) {
        this.subjectKey = subjectKey;
    }


    public Map<String, Object> getAttributes() {
        return this.attributes;
    }
    public <T> T getRequiredAttribute(String name) {
        return getAttribute(name);
    }
    public <T> T getAttribute(String name) {
        return (T) getAttributes().get(name);
    }
}
