package com.micro.platform.oauth.framework.interfaces;

import com.micro.platform.oauth.framework.domain.OauthContext;
import com.micro.platform.oauth.framework.domain.OauthRequest;

public interface ITokenGranter {
    OauthContext grant(OauthRequest request);

    String getGrantType();

    default boolean registerGrant(ITokenGranter tokenGranter){
        return Boolean.FALSE;
    }
}
