package com.micro.platform.oauth.controller.rsa;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("RSA信息响应")
public class RsaResponse {
    @ApiModelProperty("公钥")
    private String publicKey;
    @ApiModelProperty("密钥ID")
    private String rsaKeyId;
}
