package com.micro.platform.oauth.framework.constants;

public interface OauthConstants {
    int DEFAULT_ACCESS_EXPIRES_TIME = 3600 * 24;
    int DEFAULT_REFRESH_EXPIRES_TIME = 3600 * 24 * 10;

    interface TokenData {
        String KEY_IS_SUBJECT_KEY = "subjectKey";
        String KEY_IS_AUTHORITIES = "authorities";
        String KEY_IS_ACCOUNT = "account";
        String KEY_IS_CLIENT_ID = "clientId";
        String KEY_IS_PUBLIC_KEY = "publicKey";
        String KEY_IS_SCOPES = "scopes";
        String KEY_IS_ACCESS_TOKEN = "accessToken";
        String KEY_IS_REFRESH_TOKEN = "refreshToken";
        String KEY_IS_EXPIRES_TIME = "expiresTime";
        String KEY_IS_EXPIRES_TIME_STAMP = "expiresTimeStamp";
        String KEY_IS_REFRESH_EXPIRES_TIME = "refreshExpiresTime";
        String KEY_IS_REFRESH = "isRefresh";
    }
}
