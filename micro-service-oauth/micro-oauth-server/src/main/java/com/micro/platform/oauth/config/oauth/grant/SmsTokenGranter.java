package com.micro.platform.oauth.config.oauth.grant;

import com.micro.platform.oauth.framework.OauthManager;
import com.micro.platform.oauth.framework.domain.ClientDetails;
import com.micro.platform.oauth.framework.domain.OauthContext;
import com.micro.platform.oauth.framework.domain.OauthRequest;
import com.micro.platform.oauth.framework.domain.UserDetails;
import com.micro.platform.oauth.framework.interfaces.ITokenGranter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SmsTokenGranter implements ITokenGranter {
    private String grantType = "sms";

    public SmsTokenGranter(OauthManager oauthManager) {
        log.info("SmsTokenGranter created!");
    }

    @Override
    public OauthContext grant(OauthRequest request) {
        ClientDetails clientDetails = null;
        UserDetails userDetails = null;
        return new OauthContext(userDetails, request, clientDetails);
    }

    @Override
    public String getGrantType() {
        return grantType;
    }
}
