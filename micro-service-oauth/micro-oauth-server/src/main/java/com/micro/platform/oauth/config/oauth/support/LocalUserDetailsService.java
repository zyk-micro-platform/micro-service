package com.micro.platform.oauth.config.oauth.support;

import com.micro.platform.oauth.domain.LocalUserDetails;
import com.micro.platform.oauth.framework.domain.UserDetails;
import com.micro.platform.oauth.framework.exception.OauthException;
import com.micro.platform.oauth.framework.exception.OauthExceptionEnum;
import com.micro.platform.oauth.framework.interfaces.IUserDetailsService;
import com.micro.platform.user.dto.UserApisDTO;
import com.micro.platform.user.rpc.UserRpcService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Component;

@Component
public class LocalUserDetailsService implements IUserDetailsService {

    @DubboReference
    private UserRpcService userRpcService;

    @Override
    public UserDetails loadUserDetailsByAccount(String account) {
        UserApisDTO userapisDTO = userRpcService.getUserApisByAccount(account);
        if(userapisDTO == null){
            throw new OauthException(OauthExceptionEnum.UserAccountIsNotExist);
        }
        LocalUserDetails localUserDetails = new LocalUserDetails(userapisDTO);
        return localUserDetails;
    }
}
