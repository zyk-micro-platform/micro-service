package com.micro.platform.oauth.framework.interfaces;

import com.micro.platform.oauth.framework.domain.ClientDetails;

public interface IClientDetailsService {
    ClientDetails loadClientDetailsByClientId(String clientId);
}
