package com.micro.platform.oauth.domain;

import com.micro.platform.oauth.entity.OauthClient;
import com.micro.platform.oauth.framework.domain.ClientDetails;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;


public class LocalClientDetails implements ClientDetails {
    private static final long serialVersionUID = 1L;
    private String clientId;
    private String clientSecret;
    private String clientName;
    private String[] authorities;
    private String[] authorizedGrantTypes;
    private String autoApprove;
    private LocalDateTime expirationDate;
    private Integer accessTokenValidity;
    private Integer refreshTokenValidity;
    private String[] scopes;
    private String redirectUri;
    private Boolean enable;
    private String publicKey;
    private Integer secretKeyType;

    public LocalClientDetails(OauthClient oauthClient) {
        this.clientId = oauthClient.getClientId();
        this.clientSecret = oauthClient.getClientSecret();
        this.clientName = oauthClient.getClientName();
        this.authorities = Optional.ofNullable(oauthClient.getAuthorities())
                .map(authoritieStr -> Arrays.stream(authoritieStr.split(","))
                        .filter(StringUtils::isNotEmpty).toArray(String[]::new)
                ).orElse(new String[]{});
        this.authorizedGrantTypes = Optional.ofNullable(oauthClient.getAuthorizedGrantTypes())
                .map(authoritieStr -> Arrays.stream(authoritieStr.split(","))
                        .filter(StringUtils::isNotEmpty).toArray(String[]::new)
                ).orElse(new String[]{});
        this.autoApprove = oauthClient.getAutoApprove();
        this.expirationDate = oauthClient.getExpirationDate();
        this.accessTokenValidity = oauthClient.getAccessTokenValidity();
        this.refreshTokenValidity = oauthClient.getRefreshTokenValidity();
        this.scopes = Optional.ofNullable(oauthClient.getScope())
                .map(authoritieStr -> Arrays.stream(authoritieStr.split(","))
                        .filter(StringUtils::isNotEmpty).toArray(String[]::new)
                ).orElse(new String[]{});
        this.redirectUri = oauthClient.getRedirectUri();
        this.enable = oauthClient.getEnable();
        this.publicKey = oauthClient.getPublicKey();
        this.secretKeyType = oauthClient.getSecretKeyType();
    }

    @Override
    public String getClientId() {
        return this.clientId;
    }

    @Override
    public String getClientSecret() {
        return this.clientSecret;
    }

    @Override
    public String getClientName() {
        return this.clientName;
    }

    @Override
    public String[] getAuthorities() {
        return this.authorities;
    }

    @Override
    public String[] getAuthorizedGrantTypes() {
        return this.authorizedGrantTypes;
    }

    @Override
    public String getAutoApprove() {
        return this.autoApprove;
    }

    @Override
    public LocalDateTime getExpirationDate() {
        return this.expirationDate;
    }

    @Override
    public Integer getAccessTokenValidity() {
        return this.accessTokenValidity;
    }

    @Override
    public Integer getRefreshTokenValidity() {
        return this.refreshTokenValidity;
    }

    @Override
    public String[] getScopes() {
        return this.scopes;
    }

    @Override
    public String getRedirectUri() {
        return this.redirectUri;
    }

    @Override
    public Boolean isEnable() {
        return this.enable == null ? false : this.enable;
    }

    @Override
    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public Integer getSecretKeyType() {
        return secretKeyType;
    }
}
