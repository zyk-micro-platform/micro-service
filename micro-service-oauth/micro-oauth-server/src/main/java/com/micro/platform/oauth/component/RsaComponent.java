package com.micro.platform.oauth.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 保存到缓存中
 */
@Component
public class RsaComponent {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private static final int CAPTCHA_CACHE_TTL = 60;
    public static final String CAPTCHA_CACHE_KEY = "rsa:";

    @Autowired
    private StringRedisTemplate redisTemplate;

    public boolean savePrivateKey(String rsaKeyId, String value) {
        redisTemplate.opsForValue().set(CAPTCHA_CACHE_KEY + rsaKeyId, value, CAPTCHA_CACHE_TTL, TimeUnit.SECONDS);
        return true;
    }

    public String getPrivateKey(String rsaKeyId) {
        String captcha = redisTemplate.opsForValue().get(CAPTCHA_CACHE_KEY + rsaKeyId);
        return captcha;
    }

    public void removePrivateKey(String rsaKeyId) {
        redisTemplate.delete(CAPTCHA_CACHE_KEY + rsaKeyId);
    }
}
