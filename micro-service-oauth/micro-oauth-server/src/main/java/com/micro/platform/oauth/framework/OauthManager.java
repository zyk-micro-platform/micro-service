package com.micro.platform.oauth.framework;

import com.micro.platform.oauth.framework.config.properties.OauthProperties;
import com.micro.platform.oauth.framework.defaults.DefaultTokenGranter;
import com.micro.platform.oauth.framework.defaults.DefaultTokenService;
import com.micro.platform.oauth.framework.domain.*;
import com.micro.platform.oauth.framework.exception.OauthException;
import com.micro.platform.oauth.framework.exception.OauthExceptionEnum;
import com.micro.platform.oauth.framework.interfaces.IClientDetailsService;
import com.micro.platform.oauth.framework.interfaces.ITokenGranter;
import com.micro.platform.oauth.framework.interfaces.ITokenService;
import com.micro.platform.oauth.framework.interfaces.IUserDetailsService;
import com.micro.platform.starter.utils.StringUtils;


public class OauthManager {
    private ITokenGranter tokenGranter;
    private ITokenService tokenService;
    private IClientDetailsService clientDetailsService;
    private IUserDetailsService userDetailsService;
    private OauthProperties oauthProperties;
    public OauthManager() {
        this.oauthProperties = new OauthProperties();
    }

    public OauthManager(OauthProperties oauthProperties) {
        this.oauthProperties = oauthProperties;
    }

    public AccessToken login(OauthRequest request) throws OauthException {
        OauthContext context = getTokenGranter().grant(request);
        if(null == context){
            throw new OauthException(OauthExceptionEnum.Default);
        }
        AccessToken accessToken = getTokenService().createdToken(context);
        return accessToken;
    }

    public AccessToken getAccessToken(String token) throws OauthException {
        return getTokenService().getAccessToken(token);
    }

    public RefreshToken getRefreshToken(String refreshToken)  throws OauthException{
        return getTokenService().getRefreshToken(refreshToken);
    }

    public Boolean logout(String accessToken)  throws OauthException{
        if(StringUtils.isEmpty(accessToken)){
            throw new OauthException(OauthExceptionEnum.AccessTokenIsNotEmpty);
        }
        return getTokenService().removeToken(accessToken);
    }

    public boolean registerGrant(ITokenGranter tokenGranter) throws OauthException {
        return getTokenGranter().registerGrant(tokenGranter);
    }

    public String createdAuthorizationCode(Long userId, String clientId, String domain)  throws OauthException{
        ClientDetails clientDetails = getClientDetailsService().loadClientDetailsByClientId(clientId);
//        if(clientDetails.getDomains().indexOf(domain) < 0){
//            throw new OauthException(OauthExceptionEnum.DomainIsNotExist);
//        }
        AuthorizationCodeDetails authorizationCodeDetails = new AuthorizationCodeDetails(userId, clientId);
        String authorizationCode = getTokenService().createdAuthorizationCode(authorizationCodeDetails);
        if(null == authorizationCode){
            throw new OauthException(OauthExceptionEnum.AuthorizationCodeCreateFiled);
        }
        return authorizationCode;
    }

    public AuthorizationCodeDetails validateAuthorizationCode(String clientId, String ip , String authorizationCode) throws OauthException {
//        ClientDetails clientDetails = getClientDetailsService().loadClientDetailsByClientId(clientId);
//        if(clientDetails.getIps().indexOf(ip) < 0){
//            throw new OauthException(OauthExceptionEnum.IpIsNotExist);
//        }
        return getTokenService().validateAuthorizationCode(authorizationCode);
    }

    public ITokenGranter getTokenGranter() {
        if(tokenGranter == null){
            tokenGranter = new DefaultTokenGranter(this);
        }
        return tokenGranter;
    }

    public void setTokenGranter(ITokenGranter tokenGranter) {
        this.tokenGranter = tokenGranter;
    }

    public ITokenService getTokenService() {
        if(tokenService == null){
            tokenService = new DefaultTokenService(oauthProperties);
        }
        return tokenService;
    }

    public void setTokenService(ITokenService tokenService) {
        this.tokenService = tokenService;
    }

    public IClientDetailsService getClientDetailsService() {
        if(clientDetailsService == null){
            throw new OauthException(OauthExceptionEnum.ClientDetailsServiceIsNotExist);
        }
        return clientDetailsService;
    }

    public void setClientDetailsService(IClientDetailsService clientDetailsService) {
        this.clientDetailsService = clientDetailsService;
    }

    public IUserDetailsService getUserDetailsService() {
        if(userDetailsService == null){
            throw new OauthException(OauthExceptionEnum.UserDetailsServiceIsNotExist);
        }
        return userDetailsService;
    }

    public void setUserDetailsService(IUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }
}
