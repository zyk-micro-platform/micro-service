package com.micro.platform.oauth.service;

import com.micro.platform.oauth.entity.OauthClient;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * oauth-client表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
public interface OauthClientService extends IService<OauthClient> {

    OauthClient getClientByClientId(String clientId);
}
