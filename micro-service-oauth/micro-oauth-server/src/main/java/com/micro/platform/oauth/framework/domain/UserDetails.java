package com.micro.platform.oauth.framework.domain;

import java.io.Serializable;
import java.util.List;

public interface UserDetails extends Serializable {
    List<String> getAuthorities();

    String getPassword();

    String getAccount();

    boolean isEnabled();
}
