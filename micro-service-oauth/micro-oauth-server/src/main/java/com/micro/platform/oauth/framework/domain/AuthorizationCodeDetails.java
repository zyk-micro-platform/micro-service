package com.micro.platform.oauth.framework.domain;

import lombok.Data;
import org.apache.commons.collections4.MapUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
public class AuthorizationCodeDetails implements Serializable {
    private static final long serialVersionUID = -9013966439020484913L;
    private Long userId;
    private String clientId;

    public AuthorizationCodeDetails() {
    }

    public AuthorizationCodeDetails(Long userId, String clientId) {
        this.userId = userId;
        this.clientId = clientId;
    }

    public AuthorizationCodeDetails(Map<String, Object> map) {
        this.userId = MapUtils.getLong(map, "userId");
        this.clientId = MapUtils.getString(map, "clientId");
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>(8);
        map.put("userId", userId);
        map.put("clientId", clientId);
        return map;
    }

    public Long getUserId() {
        return userId;
    }

    public String getClientId() {
        return clientId;
    }
}
