package com.micro.platform.oauth.enums;


import com.micro.platform.starter.basic.BasicErrorCode;
import com.micro.platform.starter.enums.ModuleEnum;

public enum OauthErrorCodeEnum implements BasicErrorCode {
    AUTHENTICATION_ERROR(101,"认证失败"),
    AUTHORIZATION_ERROR(102,"授权失败"),
    CAPTCHA_CODE_ERROR(103,"抱歉，验证码错误"),
    ;

    private final int code;

    private final String desc;

    private final int moduleCode = ModuleEnum.OAUTH.getCode();

    OauthErrorCodeEnum(int code, String desc) {
        this.code = ERROR_CODE_LEVEL * moduleCode + code;
        this.desc = desc;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return desc;
    }

}
