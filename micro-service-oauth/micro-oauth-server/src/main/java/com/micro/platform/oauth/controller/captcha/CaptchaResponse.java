package com.micro.platform.oauth.controller.captcha;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("验证码信息响应")
public class CaptchaResponse {
    @ApiModelProperty("有效时间")
    private Integer ttl;
    @ApiModelProperty("图像ID")
    private String graphId;
    @ApiModelProperty("图像URL")
    private String graphUrl;
    @ApiModelProperty("图像BASE64")
    private String base64EncodedGraph;
    @ApiModelProperty("图像值")
    private String captcha;
}
