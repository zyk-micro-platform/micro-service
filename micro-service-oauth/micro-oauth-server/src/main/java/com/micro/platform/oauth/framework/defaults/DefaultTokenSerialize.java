package com.micro.platform.oauth.framework.defaults;

import com.micro.platform.oauth.framework.constants.OauthConstants;
import com.micro.platform.oauth.framework.domain.TokenData;
import com.micro.platform.oauth.framework.interfaces.ITokenSerialize;
import com.micro.platform.oauth.framework.interfaces.ITokenStore;
import org.apache.commons.collections4.MapUtils;

import java.util.Map;

public class DefaultTokenSerialize implements ITokenSerialize {
    private ITokenStore tokenStore;
    public DefaultTokenSerialize(ITokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }

    @Override
    public String serialize(Map<String, Object> accessTokenData) {
        TokenData tokenData = new TokenData(accessTokenData, MapUtils.getLong(accessTokenData, OauthConstants.TokenData.KEY_IS_EXPIRES_TIME));
        return tokenStore.serializeToken(tokenData);
    }

    @Override
    public Map<String, Object> reverseSerialize(String token) {
        return tokenStore.reverseSerializeToken(token);
    }

    @Override
    public String serializeRefreshToken(Map<String, Object> refreshTokenData) {
        TokenData tokenData = new TokenData(refreshTokenData, MapUtils.getLong(refreshTokenData, OauthConstants.TokenData.KEY_IS_EXPIRES_TIME));
        return tokenStore.serializeRefreshToken(tokenData);
    }

    @Override
    public Map<String, Object> reverseSerializeRefreshToken(String token) {
        return tokenStore.reverseSerializeRefreshToken(token);
    }
}
