package com.micro.platform.oauth.framework.interfaces;

import com.micro.platform.oauth.framework.domain.AccessToken;
import com.micro.platform.oauth.framework.domain.OauthContext;
import com.micro.platform.oauth.framework.domain.Subject;

public interface ISubjectHandler {
    AccessToken createdToken(Subject subject, OauthContext context);

    String getSubjectKey(OauthContext context);

    void buildSubject(AccessToken accessToken, Subject subject);
}
