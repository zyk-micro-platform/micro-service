package com.micro.platform.oauth.framework.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "oauth.config")
public class OauthProperties {
    private String model = "redis";
    private Long authorizationCodeValidityTime = 10L;
    private OauthJwtProperties jwt = new OauthJwtProperties();
    private OauthRedisProperties redis = new OauthRedisProperties();

    public enum ModeLEnum {
        MODEL_JWT("jwt"),
        MODEL_REDIS("redis");
        private String key;
        ModeLEnum(String key){
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }
    }
}
