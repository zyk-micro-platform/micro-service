package com.micro.platform.oauth.framework.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

public interface ClientDetails extends Serializable {

    String getClientId();

    String getClientSecret();

    String getClientName();

    String[] getAuthorities();

    String[] getAuthorizedGrantTypes();

    String getAutoApprove();

    LocalDateTime getExpirationDate();

    Integer getAccessTokenValidity();

    Integer getRefreshTokenValidity();

    String[] getScopes();

    String getRedirectUri();

    Boolean isEnable();

    String getPublicKey();
}
