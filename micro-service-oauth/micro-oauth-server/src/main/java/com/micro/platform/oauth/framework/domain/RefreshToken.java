package com.micro.platform.oauth.framework.domain;

import com.micro.platform.oauth.framework.constants.OauthConstants;
import com.micro.platform.starter.utils.NumberUtils;
import lombok.Data;
import org.apache.commons.collections4.MapUtils;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;

@Data
public class RefreshToken implements Serializable {
    private static final long serialVersionUID = -189413249670982008L;
    private String refreshToken;
    private Long expiresTime;
    private Long expiresTimeStamp;
    private String account;
    private String clientId;
    private String[] scopes;
    private Map<String, Object> data;

    public RefreshToken() {
    }

    public RefreshToken(String refreshToken, Map<String, Object> refreshtokenData) {
        this.refreshToken = refreshToken;
        this.data = refreshtokenData;
        this.expiresTime = MapUtils.getLong(this.data, OauthConstants.TokenData.KEY_IS_EXPIRES_TIME);
        this.expiresTimeStamp = LocalDateTime.now().plusSeconds(this.expiresTime).atZone(ZoneOffset.of("+8")).toInstant().toEpochMilli();
        this.account = MapUtils.getString(data,OauthConstants.TokenData.KEY_IS_ACCOUNT);
        this.clientId = MapUtils.getString(data,OauthConstants.TokenData.KEY_IS_CLIENT_ID);
        this.scopes = (String[]) MapUtils.getObject(data,OauthConstants.TokenData.KEY_IS_SCOPES);
    }

    public RefreshToken(Map<String, Object> data) {
        this.refreshToken = MapUtils.getString(data,OauthConstants.TokenData.KEY_IS_REFRESH_TOKEN);
        this.expiresTime = MapUtils.getLong(data,OauthConstants.TokenData.KEY_IS_EXPIRES_TIME);
        this.expiresTimeStamp = MapUtils.getLong(data,OauthConstants.TokenData.KEY_IS_EXPIRES_TIME_STAMP);
        this.account = MapUtils.getString(data,OauthConstants.TokenData.KEY_IS_ACCOUNT);
        this.clientId = MapUtils.getString(data,OauthConstants.TokenData.KEY_IS_CLIENT_ID);
        this.scopes = (String[]) MapUtils.getObject(data,OauthConstants.TokenData.KEY_IS_SCOPES);
        this.data = data;
    }

    public Map<String, Object> toMap(){
        if(data == null){
            data = new HashMap<>();
        }
        data.put(OauthConstants.TokenData.KEY_IS_REFRESH_TOKEN, refreshToken);
        data.put(OauthConstants.TokenData.KEY_IS_EXPIRES_TIME, expiresTime);
        data.put(OauthConstants.TokenData.KEY_IS_EXPIRES_TIME_STAMP, expiresTimeStamp);
        return data;
    }

    public boolean unExpires(){
        if(this.expiresTimeStamp == null){
            return false;
        }
        return NumberUtils.compare(this.expiresTimeStamp, LocalDateTime.now().atZone(ZoneOffset.of("+8")).toInstant().toEpochMilli()) > 0;
    }
}
