package com.micro.platform.oauth.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.micro.platform.oauth.entity.OauthClient;
import com.micro.platform.oauth.mapper.OauthClientMapper;
import com.micro.platform.oauth.service.OauthClientService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * oauth-client表 服务实现类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@Service
public class OauthClientServiceImpl extends ServiceImpl<OauthClientMapper, OauthClient> implements OauthClientService {

    @Override
    public OauthClient getClientByClientId(String clientId) {
        if(StringUtils.isEmpty(clientId)){
            return null;
        }
        QueryWrapper<OauthClient> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(OauthClient::getClientId, clientId);
        return this.getOne(queryWrapper, Boolean.FALSE);
    }
}
