package com.micro.platform.oauth.framework.domain;

import lombok.Getter;

import java.io.Serializable;
import java.util.Map;

@Getter
public class AuthorizationDetails implements Serializable {
    private static final long serialVersionUID = 3419021106279593346L;
    private Map<String, Object> accessTokenData;
    private Map<String, Object> refreshTokenData;
    public AuthorizationDetails(Map<String, Object> accessTokenData, Map<String, Object> refreshTokenData) {
        this.accessTokenData = accessTokenData;
        this.refreshTokenData = refreshTokenData;
    }

    public AccessToken buildAccessToken() {
        return null;
    }
}
