package com.micro.platform.oauth.framework.store.redis;

public interface IRedisSerializer {
    boolean save(Object obj, String key, Long expiresTime);
    <T> T get(String key, Class<T> classType);
    <T> T delete(String key, Class<T> classType);
}
