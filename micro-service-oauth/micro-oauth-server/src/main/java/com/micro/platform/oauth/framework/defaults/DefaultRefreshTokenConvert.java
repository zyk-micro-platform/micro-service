package com.micro.platform.oauth.framework.defaults;

import com.micro.platform.oauth.framework.constants.OauthConstants;
import com.micro.platform.oauth.framework.domain.OauthContext;
import com.micro.platform.oauth.framework.interfaces.ITokenConvert;

import java.util.Map;

public class DefaultRefreshTokenConvert implements ITokenConvert {

    @Override
    public void convert(OauthContext context, Map<String, Object> convertData) {
        convertData.put(OauthConstants.TokenData.KEY_IS_REFRESH, Boolean.TRUE);
        convertData.put(OauthConstants.TokenData.KEY_IS_EXPIRES_TIME, getRefreshTokenValidityTime(context));
    }

    @Override
    public boolean registerAccessTokenConvert(ITokenConvert tokenConvert) {
        return false;
    }

    @Override
    public boolean registerRefreshTokenConvert(ITokenConvert tokenConvert) {
        return false;
    }
}
