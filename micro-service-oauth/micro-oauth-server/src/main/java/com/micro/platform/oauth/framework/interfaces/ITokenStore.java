package com.micro.platform.oauth.framework.interfaces;

import com.micro.platform.oauth.framework.domain.*;

import java.util.Map;

public interface ITokenStore {
    boolean saveRefreshToken(RefreshToken refreshToken);
    boolean saveAccessToken(AccessToken accessToken);
    String createdAuthorizationCode(AuthorizationCodeDetails authorizationCodeDetails, Long expiresTime);

    AuthorizationCodeDetails validateAuthorizationCode(String authorizationCode);
    RefreshToken getRefreshToken(String refreshToken);
    AccessToken getAccessToken(String token);
    AccessToken removeToken(String token);
    RefreshToken removeRefreshToken(String refreshToken);

    boolean isExpired(String token);

    boolean validateToken(String token);

    boolean isExpiredRefresh(String token);

    boolean validateTokenRefresh(String token);

    String serializeToken(TokenData tokenData);

    Map<String, Object> reverseSerializeToken(String token);

    String serializeRefreshToken(TokenData tokenData);

    Map<String, Object> reverseSerializeRefreshToken(String token);

    boolean saveSubject(Subject accessToken);

    Subject getSubject(String subjectKey);

    Subject removeSubject(String subjectKey);

}
