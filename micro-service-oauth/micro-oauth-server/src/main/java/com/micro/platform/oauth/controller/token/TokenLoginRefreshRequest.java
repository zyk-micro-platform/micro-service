package com.micro.platform.oauth.controller.token;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel("刷新登录请求信息")
public class TokenLoginRefreshRequest implements Serializable {
    private static final long serialVersionUID = -6949882219841285959L;
    @ApiModelProperty("授权类型")
    @NotBlank(message = "授权类型不能为空")
    private String grantType;
    @ApiModelProperty("刷新token,grantType为refresh_token时使用")
    @NotBlank(message = "refreshToken不能为空")
    private String refreshToken;
    public TokenLoginRequest toTokenLoginRequest(){
        TokenLoginRequest tokenLoginRequest = new TokenLoginRequest();
        tokenLoginRequest.setGrantType(this.grantType);
        tokenLoginRequest.setRefreshToken(refreshToken);
        return tokenLoginRequest;
    }
}
