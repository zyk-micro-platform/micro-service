package com.micro.platform.oauth.framework.config.properties;

import java.time.Duration;
import java.util.List;

public class OauthRedisProperties {
    private String serializer = SerializerEnum.MODEL_JSON.getKey();
    private int database = 0;
    private String url;
    private String host = "localhost";
    private String password;
    private int port = 6379;
    private boolean ssl;
    private Duration timeout;
    private String clientName;
    private OauthRedisProperties.Sentinel sentinel;
    private OauthRedisProperties.Cluster cluster;
    private final OauthRedisProperties.Jedis jedis = new OauthRedisProperties.Jedis();
    private final OauthRedisProperties.Lettuce lettuce = new OauthRedisProperties.Lettuce();

    public OauthRedisProperties() {
    }

    public int getDatabase() {
        return this.database;
    }

    public void setDatabase(int database) {
        this.database = database;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isSsl() {
        return this.ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    public void setTimeout(Duration timeout) {
        this.timeout = timeout;
    }

    public String getSerializer() {
        return serializer;
    }

    public void setSerializer(String serializer) {
        this.serializer = serializer;
    }

    public Duration getTimeout() {
        return this.timeout;
    }

    public String getClientName() {
        return this.clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public OauthRedisProperties.Sentinel getSentinel() {
        return this.sentinel;
    }

    public void setSentinel(OauthRedisProperties.Sentinel sentinel) {
        this.sentinel = sentinel;
    }

    public OauthRedisProperties.Cluster getCluster() {
        return this.cluster;
    }

    public void setCluster(OauthRedisProperties.Cluster cluster) {
        this.cluster = cluster;
    }

    public OauthRedisProperties.Jedis getJedis() {
        return this.jedis;
    }

    public OauthRedisProperties.Lettuce getLettuce() {
        return this.lettuce;
    }

    public enum SerializerEnum {
        MODEL_JSON("json"),
        MODEL_BITYS("bitys");
        private String key;
        SerializerEnum(String key){
            this.key = key;
        }
        public String getKey() {
            return this.key;
        }
    }

    public static class Lettuce {
        private Duration shutdownTimeout = Duration.ofMillis(100L);
        private OauthRedisProperties.Pool pool;
        private final OauthRedisProperties.Lettuce.Cluster cluster = new OauthRedisProperties.Lettuce.Cluster();

        public Lettuce() {
        }

        public Duration getShutdownTimeout() {
            return this.shutdownTimeout;
        }

        public void setShutdownTimeout(Duration shutdownTimeout) {
            this.shutdownTimeout = shutdownTimeout;
        }

        public OauthRedisProperties.Pool getPool() {
            return this.pool;
        }

        public void setPool(OauthRedisProperties.Pool pool) {
            this.pool = pool;
        }

        public OauthRedisProperties.Lettuce.Cluster getCluster() {
            return this.cluster;
        }

        public static class Cluster {
            private final OauthRedisProperties.Lettuce.Cluster.Refresh refresh = new OauthRedisProperties.Lettuce.Cluster.Refresh();

            public Cluster() {
            }

            public OauthRedisProperties.Lettuce.Cluster.Refresh getRefresh() {
                return this.refresh;
            }

            public static class Refresh {
                private Duration period;
                private boolean adaptive;

                public Refresh() {
                }

                public Duration getPeriod() {
                    return this.period;
                }

                public void setPeriod(Duration period) {
                    this.period = period;
                }

                public boolean isAdaptive() {
                    return this.adaptive;
                }

                public void setAdaptive(boolean adaptive) {
                    this.adaptive = adaptive;
                }
            }
        }
    }

    public static class Jedis {
        private OauthRedisProperties.Pool pool;

        public Jedis() {
        }

        public OauthRedisProperties.Pool getPool() {
            return this.pool;
        }

        public void setPool(OauthRedisProperties.Pool pool) {
            this.pool = pool;
        }
    }

    public static class Sentinel {
        private String master;
        private List<String> nodes;
        private String password;

        public Sentinel() {
        }

        public String getMaster() {
            return this.master;
        }

        public void setMaster(String master) {
            this.master = master;
        }

        public List<String> getNodes() {
            return this.nodes;
        }

        public void setNodes(List<String> nodes) {
            this.nodes = nodes;
        }

        public String getPassword() {
            return this.password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public static class Cluster {
        private List<String> nodes;
        private Integer maxRedirects;

        public Cluster() {
        }

        public List<String> getNodes() {
            return this.nodes;
        }

        public void setNodes(List<String> nodes) {
            this.nodes = nodes;
        }

        public Integer getMaxRedirects() {
            return this.maxRedirects;
        }

        public void setMaxRedirects(Integer maxRedirects) {
            this.maxRedirects = maxRedirects;
        }
    }

    public static class Pool {
        private int maxIdle = 8;
        private int minIdle = 0;
        private int maxActive = 8;
        private Duration maxWait = Duration.ofMillis(-1L);
        private Duration timeBetweenEvictionRuns;

        public Pool() {
        }

        public int getMaxIdle() {
            return this.maxIdle;
        }

        public void setMaxIdle(int maxIdle) {
            this.maxIdle = maxIdle;
        }

        public int getMinIdle() {
            return this.minIdle;
        }

        public void setMinIdle(int minIdle) {
            this.minIdle = minIdle;
        }

        public int getMaxActive() {
            return this.maxActive;
        }

        public void setMaxActive(int maxActive) {
            this.maxActive = maxActive;
        }

        public Duration getMaxWait() {
            return this.maxWait;
        }

        public void setMaxWait(Duration maxWait) {
            this.maxWait = maxWait;
        }

        public Duration getTimeBetweenEvictionRuns() {
            return this.timeBetweenEvictionRuns;
        }

        public void setTimeBetweenEvictionRuns(Duration timeBetweenEvictionRuns) {
            this.timeBetweenEvictionRuns = timeBetweenEvictionRuns;
        }
    }
}
