package com.micro.platform.oauth.framework.store.jwt;

import com.micro.platform.oauth.framework.config.properties.OauthJwtProperties;
import com.micro.platform.oauth.framework.domain.TokenData;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.Map;

@Slf4j
public class JwtTemplate {

    /**
     * 秘钥
     * - 默认zhouyoukai
     */
    private String secret;

    public JwtTemplate(OauthJwtProperties jwtProperties) {
        this.secret = jwtProperties.getSecret();
    }

    /**
     * 从token中获取claim
     *
     * @param token token
     * @return claim
     */
    public Claims getClaimsFromToken(String token) {
        try {
            return Jwts.parser()
                    .setSigningKey(this.secret.getBytes())
                    .parseClaimsJws(token)
                    .getBody();
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | IllegalArgumentException e) {
            log.error("token解析错误", e);
            throw new IllegalArgumentException("Token invalided.");
        }
    }

    /**
     * 获取token的过期时间
     *
     * @param token token
     * @return 过期时间
     */
    public Date getExpirationDateFromToken(String token) {
        return getClaimsFromToken(token).getExpiration();
    }

    /**
     * 判断token是否过期
     *
     * @param token token
     * @return 已过期返回true，未过期返回false
     */
    public Boolean isTokenExpired(String token) {
        Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    /**
     * 计算token的过期时间
     *
     * @return 过期时间
     * @param expiresTime
     */
    public Date getExpirationTime(Long expiresTime) {
        return new Date(System.currentTimeMillis() + expiresTime * 1000);
    }

    /**
     * 为指定用户生成token
     *
     * @param tokenData token信息
     * @return token
     */
    public String generateToken(TokenData tokenData) {
        return generateToken(tokenData.getTokenData(), tokenData.getExpiresTime());
    }

    public String generateToken(Map<String, Object> tokenData, Long expiresTime) {
        Date createdTime = new Date();
        Date expirationTime = this.getExpirationTime(expiresTime);
        byte[] keyBytes = secret.getBytes();
        SecretKey key = Keys.hmacShaKeyFor(keyBytes);

        return Jwts.builder()
                .setClaims(tokenData)
                .setIssuedAt(createdTime)
                .setExpiration(expirationTime)
                // 你也可以改用你喜欢的算法
                // 支持的算法详见：https://github.com/jwtk/jjwt#features
                .signWith(key, SignatureAlgorithm.HS256)
                .compact();
    }
    /**
     * 判断token是否非法
     *
     * @param token token
     * @return 未过期返回true，否则返回false
     */
    public Boolean validateToken(String token) {
        return !isTokenExpired(token);
    }

//    public String sigin(String json, Long expiresTime){
//        Date expirationTime = this.getExpirationTime(expiresTime);
//        return sigin(json, expirationTime);
//    }
//
//    public String sigin(String json, Date expirationTime){
//        byte[] keyBytes = secret.getBytes();
//        SecretKey key = Keys.hmacShaKeyFor(keyBytes);
//        String jwt = Jwts.builder()
//                .setClaims((HashMap<String, Object>)JSON.parseObject(json, Map.class))
//                .setExpiration(expirationTime)
//                // 你也可以改用你喜欢的算法
//                // 支持的算法详见：https://github.com/jwtk/jjwt#features
//                .signWith(key, SignatureAlgorithm.HS256)
//                .compact();
//        String[] split = StringUtils.split(jwt, JwtParser.SEPARATOR_CHAR);
//        String sigin = Base64.getEncoder().encodeToString(String.valueOf(expirationTime.getTime()).getBytes(StandardCharsets.UTF_8));
//        if(split.length >= 3){
//            sigin = sigin + JwtParser.SEPARATOR_CHAR + split[2];
//        }
//        return sigin;
//    }
//
//    public Boolean validSigin(String json, String sigin){
//        String[] split = StringUtils.split(sigin, JwtParser.SEPARATOR_CHAR);
//        String expiresTimeBase64 = "";
//        if(split.length >= 1){
//            expiresTimeBase64 = split[0];
//        }
//        Long expiresTime = Long.valueOf(new String(Base64.getDecoder().decode(expiresTimeBase64), StandardCharsets.UTF_8));
//        if(expiresTime < System.currentTimeMillis()){
//            return Boolean.FALSE;
//        }
//        String sigin1 = sigin(json, new Date(expiresTime));
//        return sigin.equals(sigin1);
//    }
}
