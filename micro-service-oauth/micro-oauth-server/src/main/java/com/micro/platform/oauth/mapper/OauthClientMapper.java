package com.micro.platform.oauth.mapper;

import com.micro.platform.oauth.entity.OauthClient;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * oauth-client表 Mapper 接口
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@Repository
public interface OauthClientMapper extends BaseMapper<OauthClient> {

}
