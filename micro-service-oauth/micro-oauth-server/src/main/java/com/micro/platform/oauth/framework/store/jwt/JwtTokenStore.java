package com.micro.platform.oauth.framework.store.jwt;

import com.micro.platform.oauth.framework.domain.*;
import com.micro.platform.oauth.framework.interfaces.ITokenStore;

import java.util.Map;

public class JwtTokenStore implements ITokenStore {
    private JwtTemplate jwtTemplate;
    public JwtTokenStore(JwtTemplate jwtTemplate) {
        this.jwtTemplate = jwtTemplate;
    }

    @Override
    public boolean saveRefreshToken(RefreshToken refreshToken) {
        return true;
    }

    @Override
    public boolean saveAccessToken(AccessToken accessToken) {
        return Boolean.TRUE;
    }

    @Override
    public String createdAuthorizationCode(AuthorizationCodeDetails authorizationCodeDetails, Long expiresTime) {
        return jwtTemplate.generateToken(authorizationCodeDetails.toMap(), expiresTime);
    }

    @Override
    public AuthorizationCodeDetails validateAuthorizationCode(String authorizationCode) {
        return new AuthorizationCodeDetails(jwtTemplate.getClaimsFromToken(authorizationCode));
    }

    @Override
    public RefreshToken getRefreshToken(String refreshToken) {
        Map<String, Object> tokenData = jwtTemplate.getClaimsFromToken(refreshToken);
        return new RefreshToken(tokenData);
    }

    @Override
    public AccessToken getAccessToken(String token) {
        Map<String, Object> tokenData = jwtTemplate.getClaimsFromToken(token);
        return new AccessToken(tokenData);
    }

    @Override
    public AccessToken removeToken(String token) {
        Map<String, Object> tokenData = jwtTemplate.getClaimsFromToken(token);
        return new AccessToken(tokenData);
    }

    @Override
    public RefreshToken removeRefreshToken(String refreshToken) {
        Map<String, Object> tokenData = jwtTemplate.getClaimsFromToken(refreshToken);
        return new RefreshToken(tokenData);
    }

    @Override
    public boolean isExpired(String token) {
        return jwtTemplate.isTokenExpired(token);
    }

    @Override
    public boolean validateToken(String token) {
        return jwtTemplate.validateToken(token);
    }

    @Override
    public boolean isExpiredRefresh(String token) {
        return jwtTemplate.isTokenExpired(token);
    }

    @Override
    public boolean validateTokenRefresh(String token) {
        return jwtTemplate.validateToken(token);
    }

    @Override
    public String serializeToken(TokenData tokenData) {
        return jwtTemplate.generateToken(tokenData);
    }

    @Override
    public Map<String, Object> reverseSerializeToken(String token) {
        return jwtTemplate.getClaimsFromToken(token);
    }

    @Override
    public String serializeRefreshToken(TokenData tokenData) {
        return jwtTemplate.generateToken(tokenData);
    }

    @Override
    public Map<String, Object> reverseSerializeRefreshToken(String token) {
        return jwtTemplate.getClaimsFromToken(token);
    }

    @Override
    public boolean saveSubject(Subject accessToken) {
        return true;
    }

    @Override
    public Subject getSubject(String subjectKey) {
        return null;
    }

    @Override
    public Subject removeSubject(String subjectKey) {
        return null;
    }
}