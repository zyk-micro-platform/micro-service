package com.micro.platform.oauth.controller.token;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;

@Data
@ApiModel("登录请求信息")
public class TokenLoginRequest implements Serializable {
    private static final long serialVersionUID = -6949882219841285959L;
    @ApiModelProperty("授权类型")
    private String grantType;
    @ApiModelProperty("授权账户")
    private String account;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("客户端ID")
    private String clientId;
    @ApiModelProperty("客户端密钥")
    private String clientSecret;
    @ApiModelProperty("刷新token,grantType为refresh_token时使用")
    private String refreshToken;
    @ApiModelProperty("授权码,grantType为authorization_code时使用")
    private String authorization_code;
    @ApiModelProperty("额外参数,其它自定义grantType时使用")
    private HashMap<String, String> requestParam = new HashMap<>();
}
