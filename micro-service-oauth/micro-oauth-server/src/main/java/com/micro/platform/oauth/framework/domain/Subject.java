package com.micro.platform.oauth.framework.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
public class Subject implements Serializable {
    private static final long serialVersionUID = 8419455602678980171L;
    private String subjectKey;
    private String accessToken;
    private String refreshToken;
    private Long expiresTime;
    private Map<String, String> subjectInfo = new HashMap<>();

    public Subject() {
    }

}
