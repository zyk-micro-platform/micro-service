package com.micro.platform.oauth.framework.interfaces;

import com.micro.platform.oauth.framework.domain.AccessToken;
import com.micro.platform.oauth.framework.domain.AuthorizationCodeDetails;
import com.micro.platform.oauth.framework.domain.OauthContext;
import com.micro.platform.oauth.framework.domain.RefreshToken;

public interface ITokenService {

    boolean isTokenExpired(String token);

    boolean isTokenExpired(AccessToken accessToken);

    boolean validateToken(String token);

    boolean validateToken(AccessToken accessToken);

    RefreshToken getRefreshToken(String refreshToken);

    AccessToken getAccessToken(String token);

    AccessToken createdToken(OauthContext context);

    AccessToken createAccessToken(String subjectKey, OauthContext context);

    boolean removeToken(String token);

    boolean removeToken(AccessToken accessToken);

    void setSubjectHandler(ISubjectHandler subjectHandler);

    void setTokenSerialize(ITokenSerialize tokenSerialize);

    ITokenStore getTokenStore();

    void setTokenStore(ITokenStore tokenStore);

    String createdAuthorizationCode(AuthorizationCodeDetails authorizationCodeDetails);

    AuthorizationCodeDetails validateAuthorizationCode(String authorizationCode);

    boolean registerAccessTokenConvert(ITokenConvert tokenConvert);

    boolean registerRefreshTokenConvert(ITokenConvert tokenConvert);
}
