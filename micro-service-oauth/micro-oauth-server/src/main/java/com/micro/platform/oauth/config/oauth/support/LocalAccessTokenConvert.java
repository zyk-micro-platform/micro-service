package com.micro.platform.oauth.config.oauth.support;

import com.micro.platform.oauth.domain.LocalUserDetails;
import com.micro.platform.oauth.framework.domain.OauthContext;
import com.micro.platform.oauth.framework.interfaces.ITokenConvert;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Component
public class LocalAccessTokenConvert implements ITokenConvert {

    @Override
    public void convert(OauthContext context, Map<String, Object> convertData) {
        LocalUserDetails userDetails = (LocalUserDetails) context.getUserDetails();
        convertData.put("userId", userDetails.getId());
        convertData.put("nickName", userDetails.getName());
        convertData.put("phone", userDetails.getPhone());
        convertData.put("email", userDetails.getEmail());
        LocalDateTime createTime = userDetails.getCreateTime();
        if(createTime != null){
            convertData.put("createTime", createTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        }
        convertData.put("apis", userDetails.getApis());
    }


    @Override
    public boolean registerAccessTokenConvert(ITokenConvert tokenConvert) {
        return false;
    }

    @Override
    public boolean registerRefreshTokenConvert(ITokenConvert tokenConvert) {
        return false;
    }
}
