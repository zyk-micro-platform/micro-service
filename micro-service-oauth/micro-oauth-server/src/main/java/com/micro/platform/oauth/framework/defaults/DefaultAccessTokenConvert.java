package com.micro.platform.oauth.framework.defaults;

import com.micro.platform.oauth.framework.constants.OauthConstants;
import com.micro.platform.oauth.framework.domain.OauthContext;
import com.micro.platform.oauth.framework.interfaces.ITokenConvert;

import java.util.Map;

public class DefaultAccessTokenConvert implements ITokenConvert {

    @Override
    public void convert(OauthContext context, Map<String, Object> convertData) {
        convertData.put(OauthConstants.TokenData.KEY_IS_EXPIRES_TIME,getAccessTokenValidityTime(context));
        convertData.put(OauthConstants.TokenData.KEY_IS_EXPIRES_TIME,getAccessTokenValidityTime(context));
        convertData.put(OauthConstants.TokenData.KEY_IS_AUTHORITIES, context.getUserDetails().getAuthorities());
        convertData.put(OauthConstants.TokenData.KEY_IS_SUBJECT_KEY, context.getSubjectKey());
    }

    @Override
    public boolean registerAccessTokenConvert(ITokenConvert tokenConvert) {
        return false;
    }

    @Override
    public boolean registerRefreshTokenConvert(ITokenConvert tokenConvert) {
        return false;
    }
}
