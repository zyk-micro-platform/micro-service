package com.micro.platform.oauth.framework.config;

import com.micro.platform.oauth.framework.OauthManager;
import com.micro.platform.oauth.framework.config.properties.OauthProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OauthConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public OauthProperties oauthProperties() {
        return new OauthProperties();
    }

    @Bean
    @ConditionalOnMissingBean
    public OauthManager oauthManager(OauthProperties oauthProperties) {
        return new OauthManager(oauthProperties);
    }
}
