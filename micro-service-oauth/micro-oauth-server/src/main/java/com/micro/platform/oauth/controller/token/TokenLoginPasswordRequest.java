package com.micro.platform.oauth.controller.token;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel("密码登录请求信息")
public class TokenLoginPasswordRequest implements Serializable {
    private static final long serialVersionUID = -6949882219841285959L;
    @ApiModelProperty("授权类型")
    @NotBlank(message = "授权类型不能为空")
    private String grantType;
    @ApiModelProperty("授权账户")
    @NotBlank(message = "授权账户不能为空")
    private String account;
    @ApiModelProperty("密码")
    @NotBlank(message = "密码不能为空")
    private String password;
    @ApiModelProperty("客户端ID")
    @NotBlank(message = "客户端ID不能为空")
    private String clientId;
    @ApiModelProperty("客户端密钥")
    @NotBlank(message = "客户端密钥不能为空")
    private String clientSecret;
    @ApiModelProperty("验证图像ID")
    @NotBlank(message = "验证图像ID不能为空")
    private String graphId;
    @ApiModelProperty("RSA-公钥")
    @NotBlank(message = "RSA-公钥不能为空")
    private String publicKey;
    public TokenLoginRequest toTokenLoginRequest(){
        TokenLoginRequest tokenLoginRequest = new TokenLoginRequest();
        tokenLoginRequest.setGrantType(this.grantType);
        tokenLoginRequest.setAccount(this.getAccount());
        tokenLoginRequest.setPassword(this.getPassword());
        tokenLoginRequest.setClientId(this.getClientId());
        tokenLoginRequest.setClientSecret(this.getClientSecret());
        tokenLoginRequest.getRequestParam().put("graphId", graphId);
        tokenLoginRequest.getRequestParam().put("publicKey", publicKey);
        return tokenLoginRequest;
    }
}
