package com.micro.platform.oauth.framework.interfaces;

import java.util.Map;

public interface ITokenSerialize {
    String serialize(Map<String, Object> accessTokenData);
    Map<String, Object> reverseSerialize(String token);
    default String serializeRefreshToken(Map<String, Object> refreshTokenData){
        return serialize(refreshTokenData);
    }
    default Map<String, Object> reverseSerializeRefreshToken(String token){
        return reverseSerialize(token);
    }
}
