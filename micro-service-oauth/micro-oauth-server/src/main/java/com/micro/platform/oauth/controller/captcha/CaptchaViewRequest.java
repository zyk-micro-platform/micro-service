package com.micro.platform.oauth.controller.captcha;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("验证码展示请求")
public class CaptchaViewRequest {
    @ApiModelProperty("验证码图形ID")
    @NotBlank(message = "验证码图形ID不能为空")
    private String graphId;
    @ApiModelProperty("验证码图形宽")
    private Integer width = 150;
    @ApiModelProperty("验证码图形高")
    private Integer height = 38;
}
