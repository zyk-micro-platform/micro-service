package com.micro.platform.oauth.framework.domain;

import java.util.Map;

public class TokenData {
    private static final long serialVersionUID = -299504339349239610L;
    public TokenData(Map<String, Object> tokenData, Long expiresTime) {
        this.tokenData = tokenData;
        this.expiresTime = expiresTime;
    }
    private Map<String, Object> tokenData;
    private Long expiresTime;

    public Map<String, Object> getTokenData() {
        return tokenData;
    }

    public Long getExpiresTime() {
        return expiresTime;
    }

    public void setTokenData(Map<String, Object> tokenData) {
        this.tokenData = tokenData;
    }

    public void setExpiresTime(Long expiresTime) {
        this.expiresTime = expiresTime;
    }
}
