package com.micro.platform.oauth.framework.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;

@Data
public class OauthRequest implements Serializable {
    private static final long serialVersionUID = -1830717429255082293L;
    private String grantType;
    private String account;
    private String password;
    private String clientId;
    private String clientSecret;
    private String refreshToken;
    private String authorization_code;
    private HashMap<String, String> requestParam = new HashMap<>();
}
