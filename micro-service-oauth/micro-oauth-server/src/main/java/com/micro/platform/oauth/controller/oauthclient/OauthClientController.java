package com.micro.platform.oauth.controller.oauthclient;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * oauth-client表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@RestController
@RequestMapping("/oauth-client")
public class OauthClientController {

}
