package com.micro.platform.oauth.framework.defaults;

import com.micro.platform.oauth.framework.domain.AccessToken;
import com.micro.platform.oauth.framework.domain.OauthContext;
import com.micro.platform.oauth.framework.domain.Subject;
import com.micro.platform.oauth.framework.interfaces.ISubjectHandler;
import com.micro.platform.oauth.framework.interfaces.ITokenStore;
import com.micro.platform.starter.utils.StringUtils;

public class DefaultSubjectHandler implements ISubjectHandler {

    private ITokenStore tokenStore;
    public DefaultSubjectHandler(ITokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }

    @Override
    public AccessToken createdToken(Subject subject, OauthContext context) {
        String accessTokenStr = subject.getAccessToken();
        AccessToken accessToken = tokenStore.getAccessToken(accessTokenStr);
        if(accessToken != null){
            if(accessToken.unExpires()){
                return accessToken;
            }
        }
        return null;
    }

    @Override
    public String getSubjectKey(OauthContext context) {
        String subjectKey = context.getSubjectKey();
        if(StringUtils.isEmpty(subjectKey)){
            subjectKey = context.getUserDetails().getAccount();
        }
        return subjectKey;
    }

    @Override
    public void buildSubject(AccessToken accessToken, Subject subject) {

    }
}
