package com.micro.platform.oauth.controller.captcha;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
@ApiModel("验证码短信请求")
public class CaptchaSmsRequest {
    @ApiModelProperty("电话号码")
    @NotBlank(message = "电话号码不能为空")
    private String phone;
    @ApiModelProperty("验证码")
    @NotBlank(message = "验证码不能为空")
    private String captcha;
    @ApiModelProperty("验证码图形ID")
    @NotBlank(message = "验证码图形ID不能为空")
    private String graphId;
}
