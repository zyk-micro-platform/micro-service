package com.micro.platform.oauth.rpc;

import com.micro.platform.oauth.dto.TokenInfoDTO;

public interface OauthRpcService {
    TokenInfoDTO analysisAccessToken(String accessToken);
}
