package com.micro.platform.oauth.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RSAPrivateKeyDTO implements Serializable {
    private static final long serialVersionUID = -2304114567661563160L;
    private String privateKey;
}
