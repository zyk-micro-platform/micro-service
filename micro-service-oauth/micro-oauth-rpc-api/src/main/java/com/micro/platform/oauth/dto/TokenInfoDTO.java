package com.micro.platform.oauth.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class TokenInfoDTO implements Serializable {
    private static final long serialVersionUID = 3384804456883058316L;
    private Long userId;
    private String account;
    private String nickName;
    private String phone;
    private String email;
    private String privateKey;
    private String publicKey;
    private String createTime;
    private List<String> apis;
    private List<String> authorities;
}
