package com.micro.platform.oauth.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RefreshTokenInfoDTO implements Serializable {
    private static final long serialVersionUID = -5643996658625031629L;
    private String account;
    private String clientId;
    private String[] scopes;
    private String privateKey;
}
