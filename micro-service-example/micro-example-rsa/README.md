# 针对测试使用的RSA 公钥加解密服务

### POSTMAN 用法

- pre-request script
```javascript

var data = {};

// 构造一个登录请求
const loginRequest = {
    url: 'http://localhost:10910/rsa/publicEncryption',
    method: "POST",
    body: {
        // 模式为表单url编码模式
        mode: 'urlencoded',
        urlencoded: 'data=' + JSON.stringify(data) + "&publicKey=" + pm.environment.get("publicKey")
    }
};

// 发送请求
pm.sendRequest(loginRequest, function (err, res) {
    if(!err && !!res){
        let rsaBody = JSON.parse(res.text())["encryptData"];
        pm.environment.set("rsaBody", rsaBody);
    }
});

```

- Test
```javascript
// 构造一个登录请求
const loginRequest = {
    url: 'http://localhost:10910/rsa/publicDecrypt',
    method: "POST",
    body: {
        // 模式为表单url编码模式
        mode: 'urlencoded',
        urlencoded: 'data=' + responseBody + "&publicKey=" + pm.environment.get("publicKey")
    }
};

// 发送请求
pm.sendRequest(loginRequest, function (err, res) {
    var rdata = JSON.parse(res.text()).decryptData || {};
    console.info("解密数据：")
    console.info(rdata)
    if(rdata.data){
        pm.environment.set("access-token", rdata.data.accessToken);
        pm.environment.set("refresh-token", rdata.data.refreshToken);
    }
});







```