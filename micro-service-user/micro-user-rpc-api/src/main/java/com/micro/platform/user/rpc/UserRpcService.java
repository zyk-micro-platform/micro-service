package com.micro.platform.user.rpc;

import com.micro.platform.user.dto.UserApisDTO;

/**
 * <p>
 * 用户服务接口
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
public interface UserRpcService {
    UserApisDTO getUserApisByAccount(String account);
}
