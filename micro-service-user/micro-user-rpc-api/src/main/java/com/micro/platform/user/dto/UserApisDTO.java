package com.micro.platform.user.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 用户信息
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserApisDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 账号
     */
    private String account;

    private Long deptId;

    /**
     * 用户名
     */
    private String name;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 密码
     */
    private String password;

    /**
     * 账号是否可用 1-可用  0-不可用
     */
    private Boolean enable;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 是否已删除 0-未删除 1-已删除
     */
    private Integer deleted;

    /**
     * roleCodes
     */
    private List<String> roleCodes;
    /**
     * apis
     */
    private List<String> apis;
}
