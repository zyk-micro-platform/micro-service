import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;


public class Generator {

    // 工程路径
    private static final String PROJECT_PACKAGE = System.getProperty("user.dir");
    // java包路径
    private static String MODEL_JAVA_PACKAGE;
    // resources包路径
    private static String MODEL_RES_PACKAGE;
    // 模块名称
    private static final String MODEL_PATH = "micro-service-user/micro-user-server";
    // 生成文件的父包路径
    private static final String PARENT_PACKAGE = "com.micro";
    private static final String PARENT_PACKAGE_PATH = "com/micro";
    // 生成文件的业务包路径
    private static final String BUSINESS_PACKAGE = "user";
    // 要生成的数据库表名
    private static final String[] TABLE_NAMES = new String[]{
            "dept",
            "function",
            "function_api",
            "menu",
            "platform",
            "role",
            "role_function",
            "user",
            "user_role"
    };
    // 文件创建者信息
    private static final String AUTHOR_NAME = "zhouyk";

    public static void main(String[] args) {

        if (StringUtils.isEmpty(MODEL_PATH)) {
            MODEL_JAVA_PACKAGE = PROJECT_PACKAGE + "/src/main/java";
            MODEL_RES_PACKAGE = PROJECT_PACKAGE + "/src/main/resources";
        } else {
            MODEL_JAVA_PACKAGE = PROJECT_PACKAGE + "/" + MODEL_PATH + "/src/main/java";
            MODEL_RES_PACKAGE = PROJECT_PACKAGE + "/" + MODEL_PATH + "/src/main/resources";
        }

        String url = "jdbc:mysql://127.0.0.1:3306/micro_service_user?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT%2B8";
        String userName = "root";
        String password = "root";

        generate(url, userName, password);
    }

    /**
     * @param url         数据库地址
     * @param userName    用户名
     * @param password    密码
     */
    public static void generate(String url, String userName, String password) {
        // ===============  全局配置  ==================
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir(MODEL_JAVA_PACKAGE)
                .setActiveRecord(true)          // 是否支持 AR, 实体类只需继承 Model 类即可进行强大的 CRUD 操作
                .setAuthor(AUTHOR_NAME)          // 设置作者名字
                .setFileOverride(false)          // 文件覆盖(全新文件)
                .setIdType(IdType.AUTO)         // 主键策略
                .setBaseResultMap(true)         // SQL 映射文件
                .setBaseColumnList(true)        // SQL 片段
                .setServiceName("%sService")    // service的名字
                .setOpen(false);

        // =================  数据源配置   ===============
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL)
                .setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUrl(url)
                .setUsername(userName)
                .setPassword(password);

        // =================  包配置  ===================
        PackageConfig pc = new PackageConfig();
        pc.setParent(PARENT_PACKAGE)                        // 配置父包路径
                .setModuleName(BUSINESS_PACKAGE)            // 配置业务包路径
                .setMapper("mapper")
                .setEntity("entity")
                .setService("service")
                .setServiceImpl("service.impl")             // 会自动生成 impl，可以不设定
                .setServiceImpl("service.impl")             // 会自动生成 impl，可以不设定
                .setController("controller");

        // ==================  自定义配置  =================
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        List<FileOutConfig> focList = new ArrayList<>();
        // 调整 xml 生成目录演示
        focList.add(new FileOutConfig("/templates/mapper.xml.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称
                return MODEL_RES_PACKAGE + "/mybatis/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        // 调整 controller 生成目录演示
        focList.add(new FileOutConfig("/templates/controller.java.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称
                return MODEL_JAVA_PACKAGE
                        + "/" +PARENT_PACKAGE_PATH
                        + "/" + BUSINESS_PACKAGE
                        + "/" + "controller"
                        + "/" + tableInfo.getEntityName().toLowerCase()
                        + "/" + tableInfo.getEntityName() + "Controller" + StringPool.DOT_JAVA;
            }
        });
        cfg.setFileOutConfigList(focList);

        // ===================  策略配置  ==================
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel)                   // 表名命名：  underline_to_camel 底线变驼峰
                .setColumnNaming(NamingStrategy.underline_to_camel)             // 字段命名： underline_to_camel 底线变驼峰
                .setInclude(TABLE_NAMES)                                         // 需要生成的 表名
                .setCapitalMode(true)                                           // 全局大写命名 ORACLE 注意
//                .setTablePrefix(prefixTable)                                    // 去掉 表的前缀
//                 .setFieldPrefix(pc.getModuleName() + "_")                    // 去掉字段前缀
//                .setSuperEntityClass(parentPackage + ".common.basic.BasicEntity")                        // 继承类
//                .setSuperControllerClass(parentPackage + ".common.basic.BasicController")                // 继承类
//                .setSuperServiceClass("com.singleton.server.common.basic.BasicService")
//                 .setSuperEntityColumns("id")                                 // 设置超级超级列
                .setRestControllerStyle(true)                                   // @RestController
                .setEntityLombokModel(true)                                     // 是否加入lombok
                .setControllerMappingHyphenStyle(true);                         // 设置controller映射联字符

        // ==================  自定义模板配置： 默认配置位置 mybatis-plus/src/main/resources/templates  ======================
        // 放置自己项目的 src/main/resources/templates 目录下, 默认名称一下可以不配置，也可以自定义模板名称
        TemplateConfig tc = new TemplateConfig();
        tc.setXml(null)                                                         // 设置生成xml的模板
                .setEntity("/templates/entity.java")                            // 设置生成entity的模板
                .setMapper("/templates/mapper.java")                            // 设置生成mapper的模板
                .setController(null)                    // 设置生成controller的模板
                .setService("/templates/service.java")                          // 设置生成service的模板
                .setServiceImpl("/templates/serviceImpl.java");                 // 设置生成serviceImpl的模板

        // ====================  生成配置  ===================
        AutoGenerator mpg = new AutoGenerator();
        mpg.setCfg(cfg)
                .setTemplate(tc)
                .setGlobalConfig(gc)
                .setDataSource(dsc)
                .setPackageInfo(pc)
                .setStrategy(strategy)
                .setTemplateEngine(new FreemarkerTemplateEngine());             // 选择 freemarker引擎，注意 pom 依赖必须有！
        mpg.execute();
    }

}
