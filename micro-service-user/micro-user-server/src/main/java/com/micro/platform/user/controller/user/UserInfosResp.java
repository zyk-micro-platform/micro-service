package com.micro.platform.user.controller.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 用户信息响应
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
@Data
@ApiModel("用户信息响应")
public class UserInfosResp implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("账号")
    private String account;

    @ApiModelProperty("部门编号")
    private Long deptId;

    @ApiModelProperty("角色编号")
    private List<Long> roleIds;

    @ApiModelProperty("用户名")
    private String name;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("账号是否可用 1-可用  0-不可用")
    private Integer enable;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty("是否已删除 0-未删除 1-已删除")
    private Integer deleted;
}
