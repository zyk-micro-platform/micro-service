package com.micro.platform.user.controller.platform;


import com.micro.platform.starter.domain.Result;
import com.micro.platform.starter.utils.BeanCopyUtil;
import com.micro.platform.user.entity.Platform;
import com.micro.platform.user.service.PlatformService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 用户-平台表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@Api(tags = "平台-API")
@RestController
@RequestMapping("/platform")
public class PlatformController {

    @Autowired
    private PlatformService platformService;

    @ApiOperation("列表")
    @PostMapping("/list")
    public Result<List<PlatformsResp>> list(){
        List<Platform> list = platformService.list();
        if(list == null){
            return Result.ok(Collections.emptyList());
        }
        return Result.ok(BeanCopyUtil.copyListProperties(list, PlatformsResp::new));
    }

}