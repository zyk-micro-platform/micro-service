package com.micro.platform.user.service;

import com.micro.platform.user.controller.dept.DeptDeletedRequ;
import com.micro.platform.user.controller.dept.DeptEditedRequ;
import com.micro.platform.user.controller.dept.DeptInfosResp;
import com.micro.platform.user.controller.dept.DeptNewRequ;
import com.micro.platform.user.entity.Dept;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户-部门表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
public interface DeptService extends IService<Dept> {

    List<DeptInfosResp> listAll();

    Long addDept(DeptNewRequ deptNewRequ);

    Boolean edited(DeptEditedRequ deptEditedRequ);

    Boolean deleted(DeptDeletedRequ deptDeletedRequ);

    List<Dept> getChildrenDeptsById(Long deptId);
}
