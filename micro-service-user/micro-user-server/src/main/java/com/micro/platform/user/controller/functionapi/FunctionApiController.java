package com.micro.platform.user.controller.functionapi;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户-功能api关系表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@RestController
@RequestMapping("/function-api")
public class FunctionApiController {

}
