package com.micro.platform.user.controller.function;


import com.micro.platform.user.controller.functionapi.FunctionApiNewRequ;
import com.micro.platform.starter.domain.Result;
import com.micro.platform.user.service.FunctionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 用户-功能表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@Api(tags = "功能-API")
@RestController
@RequestMapping("/function")
public class FunctionController {

    @Autowired
    private FunctionService functionService;

    @ApiOperation("列表")
    @PostMapping("/list")
    public Result<List<FunctionInfosResp>> list(@RequestParam(value = "platformCode", required = false) String platformCode){
        List<FunctionInfosResp> list = functionService.listAll(platformCode);
        return Result.ok(list);
    }

    @ApiOperation("通过菜单ID获取功能信息列表")
    @PostMapping("/listByMenuId")
    public Result<List<FunctionInfosResp>> listByMenuId(@RequestParam(value = "menuId", required = false) Long menuId, @RequestParam("platformCode") String platformCode){
        List<FunctionInfosResp> list = functionService.listByMenuId(menuId, platformCode);
        return Result.ok(list);
    }

    @ApiOperation("新增")
    @PostMapping("/new")
    public Result<Boolean> newsFunction(@RequestBody @Validated(FunctionApiNewRequ.NotNullApiKeyUrl.class) FunctionNewRequ functionNewRequ){
        Boolean aBoolean = functionService.addFunction(functionNewRequ);
        return Result.ok(aBoolean);
    }

    @ApiOperation("更新")
    @PostMapping("/edited")
    public Result<Boolean> edited(@RequestBody @Validated(FunctionApiNewRequ.NotNullApiKeyUrl.class) FunctionEditedRequ functionEditedRequ){
        Boolean aBoolean = functionService.edited(functionEditedRequ);
        return Result.ok(aBoolean);
    }

    @ApiOperation("删除")
    @PostMapping("/deleted")
    public Result<Boolean> deleted(@RequestBody @Validated FunctionDeletedRequ functionDeletedRequ){
        Boolean aBoolean = functionService.deleted(functionDeletedRequ);
        return Result.ok(aBoolean);
    }
}
