package com.micro.platform.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.micro.platform.user.controller.functionapi.FunctionApiNewRequ;
import com.micro.platform.starter.utils.BeanCopyUtil;
import com.micro.platform.user.entity.FunctionApi;
import com.micro.platform.user.mapper.FunctionApiMapper;
import com.micro.platform.user.service.FunctionApiService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户-功能api关系表 服务实现类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@Service
public class FunctionApiServiceImpl extends ServiceImpl<FunctionApiMapper, FunctionApi> implements FunctionApiService {
    @Override
    public Boolean addOrUpdateBatchFunctionApi(List<FunctionApiNewRequ> functionApiNewRequs, Long functionId) {
        if(CollectionUtils.isEmpty(functionApiNewRequs) || null == functionId){
            return Boolean.TRUE;
        }
        QueryWrapper<FunctionApi> functionApiQueryWrapper = new QueryWrapper<>();
        functionApiQueryWrapper.lambda().eq(FunctionApi::getFunctionId, functionId).eq(FunctionApi::getDeleted, Boolean.FALSE);
        List<FunctionApi> allDbs = list(functionApiQueryWrapper);
        List<FunctionApi> functionApis = BeanCopyUtil.copyListProperties(functionApiNewRequs, FunctionApi::new);
        ArrayList<FunctionApi> adds = new ArrayList<>();
        ArrayList<FunctionApi> updates = new ArrayList<>();
        Map<Long, FunctionApi> mapId2FunctionApi = allDbs.stream().collect(Collectors.toMap(FunctionApi::getId, Function.identity()));
        if(CollectionUtils.isEmpty(allDbs)){
            adds.addAll(functionApis);
        }

        functionApis.forEach(functionApi -> {
            FunctionApi functionApi1 = mapId2FunctionApi.get(functionApi.getId());
            if(functionApi1 == null){
                adds.add(functionApi);
            }else{
                updates.add(functionApi);
            }
        });

        Set<Long> dbIds = mapId2FunctionApi.keySet();
        Set<Long> ids = functionApis.stream().map(FunctionApi::getId).filter(Objects::nonNull).collect(Collectors.toSet());
        dbIds.removeAll(ids);

        boolean result = true;
        if(dbIds.size() > 0){
            UpdateWrapper<FunctionApi> functionApiUpdateWrapper = new UpdateWrapper<>();
            functionApiUpdateWrapper
                    .lambda()
                    .in(FunctionApi::getId, dbIds)
                    .set(FunctionApi::getDeleted, Boolean.TRUE);
            result = this.update(functionApiUpdateWrapper);
        }
        if(adds.size() > 0){
            result = result && saveBatch(adds);
        }
        if(updates.size() > 0){
            result = result && updateBatchById(updates);
        }
        return result;
    }

    @Override
    public Boolean addBatchFunctionApi(List<FunctionApiNewRequ> functionApiNewRequs, Long functionId) {
        if(CollectionUtils.isEmpty(functionApiNewRequs) || null == functionId){
            return Boolean.TRUE;
        }
        if(null != functionId){
            this.deletedBatchFunctionApiByFunctionId(functionId);
        }
        List<FunctionApi> functionApis = BeanCopyUtil.copyListProperties(functionApiNewRequs, FunctionApi::new);
        return saveBatch(functionApis);
    }

    @Override
    public Boolean deletedBatchFunctionApiByFunctionId(Long functionId) {
        if(null == functionId){
            return Boolean.TRUE;
        }
        UpdateWrapper<FunctionApi> functionApiUpdateWrapper = new UpdateWrapper<>();
        functionApiUpdateWrapper.lambda().eq(FunctionApi::getFunctionId, functionId).set(FunctionApi::getDeleted, Boolean.TRUE);
        return this.update(functionApiUpdateWrapper);
    }

    @Override
    public Boolean updateBatchFunctionApi(List<FunctionApiNewRequ> functionApiNewRequs, Long updateId) {
        if(null != updateId){
            this.deletedBatchFunctionApiByFunctionId(updateId);
        }
        if(CollectionUtils.isEmpty(functionApiNewRequs)){
            return Boolean.TRUE;
        }
        List<FunctionApi> functionApis = BeanCopyUtil.copyListProperties(functionApiNewRequs, FunctionApi::new);
        return saveBatch(functionApis);
    }

    @Override
    public List<FunctionApi> getFunctionApisByFunctionIds(List<Long> functionIds) {
        if(CollectionUtils.isEmpty(functionIds)){
            return Collections.EMPTY_LIST;
        }
        QueryWrapper<FunctionApi> functionApiQueryWrapper = new QueryWrapper<>();
        functionApiQueryWrapper.lambda().in(FunctionApi::getFunctionId, functionIds).eq(FunctionApi::getDeleted, Boolean.FALSE);
        return list(functionApiQueryWrapper);
    }

}
