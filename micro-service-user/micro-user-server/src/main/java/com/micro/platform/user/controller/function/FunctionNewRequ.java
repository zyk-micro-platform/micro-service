package com.micro.platform.user.controller.function;

import com.micro.platform.user.controller.functionapi.FunctionApiNewRequ;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("功能新增请求")
public class FunctionNewRequ implements Serializable{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("功能名称")
    @NotEmpty(message = "功能名称不能为空!")
    private String name;

    @ApiModelProperty("平台CODE")
    @NotEmpty(message = "平台CODE不能为空!")
    private String platformCode;

    /**
     * 所属菜单
     */
    @ApiModelProperty("所属菜单ID")
    @NotNull(message = "所属菜单ID不能为空!")
    private Long menuId;

    @Valid
    @ApiModelProperty("功能Api列表")
    private List<FunctionApiNewRequ> functionApiNewRequs;
}
