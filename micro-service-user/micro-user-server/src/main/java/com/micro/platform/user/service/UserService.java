package com.micro.platform.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.micro.platform.starter.domain.PageFilter;
import com.micro.platform.starter.domain.PageResult;
import com.micro.platform.user.controller.user.*;
import com.micro.platform.user.entity.User;

/**
 * <p>
 * 用户-用户表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
public interface UserService extends IService<User> {

    Boolean addUser(UserNewRequ userNewRequ);

    PageResult<UserInfosResp> pageFilter(PageFilter<UserPageFilterRequ> pageFilter);

    Boolean edited(UserEditedRequ userEditedRequ);

    Boolean deleted(UserDeletedRequ userDeletedRequ);

    User getUserByAccount(String account);
}
