package com.micro.platform.user.controller.menu;


import com.micro.platform.starter.domain.Result;
import com.micro.platform.user.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 用户-菜单表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@Api(tags = "菜单-API")
@RestController
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private MenuService menuService;

    @ApiOperation("树")
    @PostMapping("/tree")
    public Result<List<MenuInfosResp>> tree(@RequestParam("platformCode") String platformCode){
        List<MenuInfosResp> list = menuService.tree(platformCode);
        return Result.ok(list);
    }
    @ApiOperation("同步")
    @PostMapping("/sync")
    public Result<Boolean> syncMenuInfos(@RequestBody @Validated List<MenuSyncRequ> menuSyncRequs, @RequestParam("platformCode") String platformCode){
        Boolean aBoolean = menuService.syncMenuInfos(menuSyncRequs, platformCode);
        return Result.ok(aBoolean);
    }
    @ApiOperation("列表")
    @PostMapping("/list")
    public Result<List<MenuInfosResp>> list(@RequestParam("platformCode") String platformCode){
        List<MenuInfosResp> list = menuService.listAll(platformCode);
        return Result.ok(list);
    }

    @ApiOperation(value = "菜单")
    @RequestMapping(value = "/current", method = RequestMethod.POST)
    @ResponseBody
    public Result<List<MenuInfosResp>> getMenus(@RequestParam("platformCode") String platformCode){
        List<MenuInfosResp> list = menuService.treeByUserId(1L, platformCode);
        return Result.ok(list);
    }

}