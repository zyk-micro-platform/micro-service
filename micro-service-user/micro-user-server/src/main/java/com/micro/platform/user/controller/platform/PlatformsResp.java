package com.micro.platform.user.controller.platform;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("平台信息响应")
public class PlatformsResp implements Serializable{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("平台CODE")
    private String code;

    @ApiModelProperty("平台名称")
    private String name;
}
