package com.micro.platform.user.service;

import com.micro.platform.user.controller.menu.MenuInfosResp;
import com.micro.platform.user.controller.menu.MenuSyncRequ;
import com.micro.platform.user.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户-菜单表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
public interface MenuService extends IService<Menu> {

    List<MenuInfosResp> listAll(String platformCode);

    List<Menu> list(String platformCode);

    List<MenuInfosResp> tree(String platformCode);

    Boolean syncMenuInfos(List<MenuSyncRequ> menuSyncRequs, String platformCode);

    List<Menu> getMenuTreeNodesById(Long menuId, String platformCode);

    List<MenuInfosResp> treeByUserId(Long userId, String platformCode);

}
