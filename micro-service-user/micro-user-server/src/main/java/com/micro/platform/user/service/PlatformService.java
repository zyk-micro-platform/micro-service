package com.micro.platform.user.service;

import com.micro.platform.user.entity.Platform;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户-平台表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
public interface PlatformService extends IService<Platform> {

}
