package com.micro.platform.user.service;

import com.micro.platform.user.controller.functionapi.FunctionApiNewRequ;
import com.micro.platform.user.entity.FunctionApi;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户-功能api关系表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
public interface FunctionApiService extends IService<FunctionApi> {
    Boolean addOrUpdateBatchFunctionApi(List<FunctionApiNewRequ> functionApiNewRequs, Long functionId);

    Boolean addBatchFunctionApi(List<FunctionApiNewRequ> functionApiNewRequs, Long functionId);

    Boolean deletedBatchFunctionApiByFunctionId(Long functionId);

    Boolean updateBatchFunctionApi(List<FunctionApiNewRequ> functionApiNewRequs, Long updateId);

    List<FunctionApi> getFunctionApisByFunctionIds(List<Long> functionIds);

}
