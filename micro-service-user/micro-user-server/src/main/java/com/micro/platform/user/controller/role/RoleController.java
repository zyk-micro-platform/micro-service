package com.micro.platform.user.controller.role;


import com.micro.platform.starter.domain.PageFilter;
import com.micro.platform.starter.domain.PageResult;
import com.micro.platform.starter.domain.Result;
import com.micro.platform.user.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 用户-角色表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@Api(tags = "角色-API")
@RestController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @ApiOperation("新增")
    @PostMapping("/new")
    public Result<Boolean> newRole(@RequestBody @Validated RoleNewRequ roleNewRequ) {
        Boolean aBoolean = roleService.addRole(roleNewRequ);
        return Result.ok(aBoolean);
    }

    @ApiOperation("分页过滤")
    @PostMapping("/pageFilter")
    public Result<PageResult<RoleInfosResp>> pageFilter(@RequestBody PageFilter<RolePageFilterRequ> pageFilter) {
        PageResult<RoleInfosResp> page = roleService.pageFilter(pageFilter);
        return Result.ok(page);
    }

    @ApiOperation("列表")
    @PostMapping("/list")
    public Result<List<RoleInfosResp>> list() {
        List<RoleInfosResp> list = roleService.listAll();
        return Result.ok(list);
    }

    @ApiOperation("更新")
    @PostMapping("/edited")
    public Result<Boolean> edited(@RequestBody @Validated RoleEditedRequ roleEditedRequ) {
        Boolean aBoolean = roleService.edited(roleEditedRequ);
        return Result.ok(aBoolean);
    }

    @ApiOperation("删除")
    @PostMapping("/deleted")
    public Result<Boolean> deleted(@RequestBody @Validated RoleDeletedRequ roleDeletedRequ) {
        Boolean aBoolean = roleService.deleted(roleDeletedRequ);
        return Result.ok(aBoolean);
    }
}