package com.micro.platform.user.controller.userrole;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户-用户角色关联表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@RestController
@RequestMapping("/user/user-role")
public class UserRoleController {

}
