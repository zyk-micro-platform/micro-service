package com.micro.platform.user.controller.rolefunction;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户-角色功能表 前端控制器
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@RestController
@RequestMapping("/role-function")
public class RoleFunctionController {

}
