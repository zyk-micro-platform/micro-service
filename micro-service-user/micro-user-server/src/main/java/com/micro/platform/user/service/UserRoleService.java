package com.micro.platform.user.service;

import com.micro.platform.user.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户-用户角色关联表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
public interface UserRoleService extends IService<UserRole> {
    UserRole getUserRoleByRoleId(Long roleId);

    List<UserRole> getUserRolesByUserIds(List<Long> userIds);


    Boolean deletedUserRoleByUserId(Long userId);

    List<UserRole> getUserRolesByUserId(Long userId);

    Boolean UpdateBatch(List<UserRole> lstUserRole, Long userId);
}
