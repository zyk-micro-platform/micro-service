package com.micro.platform.user.service.impl;

import com.micro.platform.user.entity.Platform;
import com.micro.platform.user.mapper.PlatformMapper;
import com.micro.platform.user.service.PlatformService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-平台表 服务实现类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@Service
public class PlatformServiceImpl extends ServiceImpl<PlatformMapper, Platform> implements PlatformService {

}
