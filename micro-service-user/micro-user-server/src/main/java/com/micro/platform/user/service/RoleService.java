package com.micro.platform.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.micro.platform.starter.domain.PageFilter;
import com.micro.platform.starter.domain.PageResult;
import com.micro.platform.user.controller.role.*;
import com.micro.platform.user.entity.Role;

import java.util.List;

/**
 * <p>
 * 用户-角色表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
public interface RoleService extends IService<Role> {

    List<Role> getRolesByUserId(Long userId);

    List<Role> getRolesByIds(List<Long> roleIds);

    Boolean addRole(RoleNewRequ roleNewRequ);

    PageResult<RoleInfosResp> pageFilter(PageFilter<RolePageFilterRequ> pageFilter);

    Boolean edited(RoleEditedRequ roleEditedRequ);

    Boolean deleted(RoleDeletedRequ roleDeletedRequ);

    List<RoleInfosResp> listAll();

    List<Role> getRolesByRoleCode(String roleCode);

}
