package com.micro.platform.user.service;

import com.micro.platform.user.controller.function.FunctionDeletedRequ;
import com.micro.platform.user.controller.function.FunctionEditedRequ;
import com.micro.platform.user.controller.function.FunctionInfosResp;
import com.micro.platform.user.controller.function.FunctionNewRequ;
import com.micro.platform.user.entity.Function;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户-功能表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
public interface FunctionService extends IService<Function> {

    Boolean addFunction(FunctionNewRequ functionNewRequ);

    List<FunctionInfosResp> listAll(String platformCode);

    List<FunctionInfosResp> listByMenuId(Long menuId, String platformCode);

    Boolean edited(FunctionEditedRequ functionEditedRequ);

    Boolean deleted(FunctionDeletedRequ functionDeletedRequ);

    Boolean deletedByMenuId(Long menuId);

    List<Function> listByIds(List<Long> functionIds, String platformCode);

}
