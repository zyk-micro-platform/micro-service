package com.micro.platform.user.service;

import com.micro.platform.user.entity.RoleFunction;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户-角色功能表 服务类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
public interface RoleFunctionService extends IService<RoleFunction> {

    Boolean removeRoleFunctionByRoleId(Long roleId);

    List<RoleFunction> getRoleFunctionsByRoleIds(List<Long> roleIds);

}
