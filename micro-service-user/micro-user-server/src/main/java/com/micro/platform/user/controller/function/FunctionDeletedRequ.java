package com.micro.platform.user.controller.function;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel("功能删除请求")
public class FunctionDeletedRequ implements Serializable{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @NotNull(message = "ID不能为空!")
    private Long id;
}
