package com.micro.platform.user.controller.dept;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@ApiModel("部门新增请求")
public class DeptNewRequ implements Serializable{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("部门名称")
    @NotEmpty(message = "部门名称不能为空!")
    private String name;

    @ApiModelProperty("父ID")
    private Long pId;
}
