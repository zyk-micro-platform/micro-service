package com.micro.platform.user.controller.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 用户分页过滤请求
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
@Data
@ApiModel("用户分页过滤请求信息")
public class UserDeletedRequ implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("主键")
    @NotNull(message = "ID不能为空!")
    private Long id;
}
