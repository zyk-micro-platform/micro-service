package com.micro.platform.user.controller.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 角色分页过滤请求
 * </p>
 *
 * @author zhouyk
 * @since 2021-09-23
 */
@Data
@ApiModel("角色分页过滤请求信息")
public class RolePageFilterRequ implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("角色类型（0：业务角色，1: 管理角色）")
    private Integer type;

    @ApiModelProperty("角色名称")
    private String name;
}
