package com.micro.platform.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户-菜单表
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Menu extends Model<Menu> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 平台CODE
     */
    private String platformCode;

    /**
     * 位置
     */
    private Long treeIndex;

    /**
     * 父位置
     */
    private Long pIndex;

    /**
     * 路由Key
     */
    private String routeKey;

    /**
     * 菜单名称
     */
    private String title;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 菜单元数据
     */
    private String meta;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 已删除 1-已删除 0-未删除
     */
    private Boolean deleted;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
