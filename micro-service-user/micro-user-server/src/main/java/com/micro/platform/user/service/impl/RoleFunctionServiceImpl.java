package com.micro.platform.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.micro.platform.user.entity.RoleFunction;
import com.micro.platform.user.mapper.RoleFunctionMapper;
import com.micro.platform.user.service.RoleFunctionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 用户-角色功能表 服务实现类
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@Service
public class RoleFunctionServiceImpl extends ServiceImpl<RoleFunctionMapper, RoleFunction> implements RoleFunctionService {


    @Override
    public Boolean removeRoleFunctionByRoleId(Long roleId) {
        if(roleId == null){
            return Boolean.TRUE;
        }
        UpdateWrapper<RoleFunction> roleFunctionUpdateWrapper = new UpdateWrapper<>();
        roleFunctionUpdateWrapper
                .lambda()
                .eq(RoleFunction::getRoleId, roleId)
                .set(RoleFunction::getDeleted, Boolean.TRUE);
        return update(roleFunctionUpdateWrapper);
    }

    @Override
    public List<RoleFunction> getRoleFunctionsByRoleIds(List<Long> roleIds) {
        if(CollectionUtils.isEmpty(roleIds)){
            return Collections.EMPTY_LIST;
        }
        QueryWrapper<RoleFunction> queryWrapper = new QueryWrapper<>();
        queryWrapper
                .lambda()
                .in(RoleFunction::getRoleId, roleIds)
                .eq(RoleFunction::getDeleted, Boolean.FALSE);
        return this.list(queryWrapper);
    }
}
