package com.micro.platform.user.mapper;

import com.micro.platform.user.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户-菜单表 Mapper 接口
 * </p>
 *
 * @author zhouyk
 * @since 2021-12-06
 */
@Repository
public interface MenuMapper extends BaseMapper<Menu> {

}
