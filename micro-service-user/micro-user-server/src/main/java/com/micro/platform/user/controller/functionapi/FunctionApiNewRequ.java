package com.micro.platform.user.controller.functionapi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel("功能API新增请求")
public class FunctionApiNewRequ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("功能ID")
    @NotNull(message = "功能ID不能为空!")
    private Long functionId;

    @ApiModelProperty("api地址")
    @NotEmpty(message = "api地址不能为空!", groups = NotNullApiKeyUrl.class)
    private String apiUrl;

    @ApiModelProperty("api描述")
    @NotEmpty(message = "api描述不能为空!", groups = NotNullApiKeyUrl.class)
    private String apiDes;

    public static interface NotNullApiKeyUrl {
    }

}
