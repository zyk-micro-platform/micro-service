package com.micro.platform.starter.utils;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public final class OptionalPlus<T> {

    private static final OptionalPlus<?> EMPTY = new OptionalPlus<>();

    private final T value;

    private OptionalPlus() {
        this.value = null;
    }

    public static<T> OptionalPlus<T> empty() {
        @SuppressWarnings("unchecked")
        OptionalPlus<T> t = (OptionalPlus<T>) EMPTY;
        return t;
    }

    private OptionalPlus(T value) {
        this.value = Objects.requireNonNull(value);
    }

    public static <T> OptionalPlus<T> of(T value) {
        return new OptionalPlus<>(value);
    }

    public static <T> OptionalPlus<T> ofNullable(T value) {
        return value == null ? empty() : of(value);
    }

    public T get() {
        if (value == null) {
            throw new NoSuchElementException("No value present");
        }
        return value;
    }

    public boolean isPresent() {
        return value != null;
    }

    public void ifPresent(Consumer<? super T> consumer) {
        if (value != null && consumer != null){
            consumer.accept(value);
        }
    }

    public OptionalPlus<T> filter(Predicate<? super T> predicate) {
        Objects.requireNonNull(predicate);
        if (!isPresent())
        {
            return this;
        }
        return predicate.test(value) ? this : empty();
    }

    public<U> OptionalPlus<U> map(Function<? super T, ? extends U> mapper) {
        Objects.requireNonNull(mapper);
        if (!isPresent()){
            return empty();
        }
        return OptionalPlus.ofNullable(mapper.apply(value));
    }

    public<U> OptionalPlus<U> flatMap(Function<? super T, OptionalPlus<U>> mapper) {
        Objects.requireNonNull(mapper);
        if (!isPresent()) {
            return empty();
        }
        return Objects.requireNonNull(mapper.apply(value));
    }

    public T orElse(T other) {
        return value != null ? value : other;
    }

    public T orElseGet(Supplier<? extends T> other) {
        return value != null ? value : other.get();
    }

    public OptionalPlus<T> doNull(Consumer<? super T> action){
        if (!this.isPresent()){
            action.accept(value);
        }
        return this;
    }

    public OptionalPlus<T> doNotNull(Consumer<? super T> action){
        if (this.isPresent()){
            action.accept(value);
        }
        return this;
    }

    public OptionalPlus<T> doCustom(Consumer<? super T> action){
        action.accept(value);
        return this;
    }

    public OptionalPlus<T> doEquals(Object obj, Consumer<? super T> action){
        if (this.isPresent() && this.value.equals(obj)){
            action.accept(value);
        }
        return this;
    }

    public <R> OptionalPlus<T> doPropertyNull(Function<? super T, ? extends R> column, Consumer<? super T> action){
        Objects.requireNonNull(column);
        if (this.isPresent()){
            R apply = column.apply(this.value);
            if(apply == null){
                action.accept(this.value);
            }
        }
        return this;
    }

    public <R> OptionalPlus<T> doPropertyNotNull(Function<? super T, ? extends R> column, Consumer<? super T> action){
        Objects.requireNonNull(column);
        if (this.isPresent()){
            R apply = column.apply(this.value);
            if(apply != null){
                action.accept(this.value);
            }
        }
        return this;
    }

    public OptionalPlus<T> doPropertyCustom(Predicate<? super T> predicate, Consumer<? super T> action){
        if(predicate == null){
            action.accept(this.value);
        }else{
            if (predicate.test(this.value)){
                action.accept(this.value);
            }
        }
        return this;
    }

    public <R> OptionalPlus<T> doPropertyEquals(Function<? super T, ? extends R> column, Object obj, Consumer<? super T> action){
        Objects.requireNonNull(column);
        if (this.isPresent()){
            R apply = column.apply(this.value);
            if (Objects.equals(apply, obj)){
                action.accept(value);
            }
        }
        return this;
    }

    public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier) throws X {
        if (value != null) {
            return value;
        } else {
            throw exceptionSupplier.get();
        }
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof OptionalPlus)) {
            return false;
        }

        OptionalPlus<?> other = (OptionalPlus<?>) obj;
        return Objects.equals(value, other.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    @Override
    public String toString() {
        return value != null ? String.format("OptionalPlus[%s]", value) : "OptionalPlus.empty";
    }

//    public static void main(String[] args) {
//        OptionalPlus.ofNullable("1")
//                .doNull(value -> System.out.println("doNull:" + value))
//                .doCustom(value -> System.out.println("doCustom:" + value))
//                .doEquals("1", value -> System.out.println("doEquals 1:" + value))
//                .doEquals("2", value -> System.out.println("doEquals 2:" + value))
//                .doNotNull(value -> System.out.println("doNotNull:" + value));
//        System.out.println("=================================================");
//        OptionalPlus.ofNullable(null)
//                .doNull(value -> System.out.println("doNull:" + value))
//                .doCustom(value -> System.out.println("doCustom:" + value))
//                .doEquals("1", value -> System.out.println("doEquals 1:" + value))
//                .doEquals("2", value -> System.out.println("doEquals 2:" + value))
//                .doNotNull(value -> System.out.println("doNotNull:" + value));
//        System.out.println("=================================================");
//        Page page = new Page();
//        OptionalPlus.ofNullable(page)
//                .doNull(value -> System.out.println("doNull:" + value))
//                .doPropertyCustom(Objects::nonNull, value -> System.out.println("nonNull doPropertyCustom:" + value))
//                .doPropertyCustom(Objects::isNull, value -> System.out.println("isNull doPropertyCustom:" + value))
//                .doPropertyNull(Page::getPageSize, value -> System.out.println("getPageSize doPropertyNull:" + value))
//                .doPropertyNotNull(Page::getPageSize, value -> System.out.println("getPageSize doPropertyNotNull:" + value))
//                .doPropertyEquals(Page::getPageSize, 10, value -> System.out.println("getPageSize 10 doPropertyEquals:" + value))
//                .doPropertyEquals(Page::getPageSize, 20, value -> System.out.println("getPageSize 20 doPropertyEquals:" + value))
//                .orElseGet(() -> new Page());
//        System.out.println("=================================================");
//        Page pageNull = null;
//        Page page1 = OptionalPlus.ofNullable(pageNull)
//                .doNull(value -> System.out.println("doNull:" + value))
//                .doPropertyCustom(Objects::nonNull, value -> System.out.println("nonNull doPropertyCustom:" + value))
//                .doPropertyCustom(Objects::isNull, value -> System.out.println("isNull doPropertyCustom:" + value))
//                .doPropertyNull(Page::getPageSize, value -> System.out.println("getPageSize doPropertyNull:" + value))
//                .doPropertyNotNull(Page::getPageSize, value -> System.out.println("getPageSize doPropertyNotNull:" + value))
//                .doPropertyEquals(Page::getPageSize, 10, value -> System.out.println("getPageSize 10 doPropertyEquals:" + value))
//                .doPropertyEquals(Page::getPageSize, 20, value -> System.out.println("getPageSize 20 doPropertyEquals:" + value))
//                .orElseGet(() -> new Page());
//        System.out.println(page1);
//    }
}
