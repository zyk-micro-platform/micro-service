package com.micro.platform.starter.config.loadbalance.ribbon;

import com.netflix.loadbalancer.IRule;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RibbonClients(defaultConfiguration = NacosExtendRule.class)
public class BalancerRuleConfig {
    @Bean
    public IRule ribbonRule() {
        return new NacosExtendRule();
    }
}
