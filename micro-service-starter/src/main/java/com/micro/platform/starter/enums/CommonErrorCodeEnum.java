package com.micro.platform.starter.enums;


import com.micro.platform.starter.basic.BasicErrorCode;

public enum CommonErrorCodeEnum implements BasicErrorCode {
    SERVER_ERROR(101,"服务出错"),
    BASE_PARAM_ERROR(102,"参数校验不通过"),
    BASE_BAD_REQUEST_ERROR(103,"无效的请求"),
    SERVER_NOT_FOUND(104,"服务未发现"),
    SERVER_METHOD_NOT_ALLOWED(105,"方法不允许"),
    ;

    private final int code;

    private final String desc;

    private final int moduleCode = ModuleEnum.COMMON.getCode();

    CommonErrorCodeEnum(int code, String desc) {
        this.code = ERROR_CODE_LEVEL * moduleCode + code;
        this.desc = desc;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return desc;
    }

}
