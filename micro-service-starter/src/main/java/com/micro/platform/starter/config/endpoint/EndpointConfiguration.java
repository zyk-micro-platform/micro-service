package com.micro.platform.starter.config.endpoint;


import com.micro.platform.starter.config.api.ApiBuildConfiguration;
import com.micro.platform.starter.config.api.ApiBuildHandler;
import com.micro.platform.starter.endpoint.ApiEndpoint;
import org.springframework.boot.actuate.autoconfigure.endpoint.condition.ConditionalOnAvailableEndpoint;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@ConditionalOnClass(Endpoint.class)
@Configuration(proxyBeanMethods = false)
@AutoConfigureBefore(ApiBuildConfiguration.class)
public class EndpointConfiguration {

    @Bean
    @ConditionalOnAvailableEndpoint
    public ApiEndpoint apiEndpoint(ApiBuildHandler apiBuildHandler) {
        return new ApiEndpoint(apiBuildHandler);
    }

}
