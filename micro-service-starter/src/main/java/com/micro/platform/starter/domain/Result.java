package com.micro.platform.starter.domain;

import com.micro.platform.starter.basic.BasicErrorCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * 全局返回结果集
 *
 * @param <T>
 */
@Data
@ApiModel("响应信息")
public class Result<T> {
    private static int ARGUMENT_NOT_VALID_EXCEPTION = 9999;
    @ApiModelProperty("响应码")
    private int code;
    @ApiModelProperty("响应提示")
    private String message;
    @ApiModelProperty("响应数据")
    private T data;
    @ApiModelProperty("堆栈信息")
    private String stack;

    public static <T> Result<T> ok(T body) {
        Result<T> result = new Result<T>();
        result.setCode(HttpStatus.OK.value());
        result.setData(body);
        return result;
    }

    public static <T> Result<T> ok() {
        Result<T> result = new Result<T>();
        result.setCode(HttpStatus.OK.value());
        return result;
    }

    public static <T> Result<T> ok(String message) {
        Result<T> result = new Result<T>();
        result.setCode(HttpStatus.OK.value());
        result.setMessage(message);
        return result;
    }

    public static <T> Result<T> ok(String message, T body) {
        Result<T> result = new Result<T>();
        result.setCode(HttpStatus.OK.value());
        result.setMessage(message);
        result.setData(body);
        return result;
    }

    public static <T> Result<T> error(BasicErrorCode basicErrorCode) {
        Result<T> result = new Result<T>();
        result.setCode(basicErrorCode.getCode());
        result.setMessage(basicErrorCode.getMessage());
        result.setStack(basicErrorCode.getStack());
        return result;
    }
}
