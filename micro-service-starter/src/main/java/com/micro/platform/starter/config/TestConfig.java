package com.micro.platform.starter.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

/**
 * test:
 *   condition:
 *     expre: true
 */
@Component
@ConditionalOnExpression("#{'true'.equals(environment['test.condition.expre'])}")
public class TestConfig {
    public TestConfig() {
        System.out.println("-------------------------111111111111111111111----------------------------");
    }

    public static void main(String[] args) {
        System.out.println(new String("null").indexOf('1') > -1);
    }
}
