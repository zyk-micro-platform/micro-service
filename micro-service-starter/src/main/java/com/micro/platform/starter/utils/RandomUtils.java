package com.micro.platform.starter.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;
import java.util.UUID;

/**
 * 随机数工具类
 */
public class RandomUtils {
    /***
     * 生成随机数
     * @param count
     * @return java.lang.String
     **/
    public static String randomNumeric(int count) {
        return RandomStringUtils.randomNumeric(count);
    }

    /***
     * 生成uuid
     * @param
     * @return java.lang.String
     **/
    public static String uuid() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replace("-", "");
    }
    /**
     *  随机生成指定位数的字母数字组合
     *  @Param  [length]
     *  @Return      java.lang.String
     *  @Exception
     */
    public static String getCharAndNumr(int length) {
        String val = "";
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            // 输出字母还是数字
            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
            // 字符串
            if ("char".equalsIgnoreCase(charOrNum)) {
                // 取得大写字母还是小写字母
                int choice = random.nextInt(2) % 2 == 0 ? 65 : 97;
                val += (char) (choice + random.nextInt(26));
                // 数字
            } else {
                val += String.valueOf(random.nextInt(10));
            }
        }
        return val;
    }
}
