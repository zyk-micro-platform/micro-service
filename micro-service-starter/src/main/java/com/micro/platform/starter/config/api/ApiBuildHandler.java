package com.micro.platform.starter.config.api;

import com.micro.platform.starter.domain.ApiMateInfo;

import java.util.List;
import java.util.regex.Pattern;

public interface ApiBuildHandler {
    List<ApiMateInfo> buildApis();

    /**
     * 验证包路径
     *
     * @param classPackage 需要验证的包路径
     * @param scanPackages 验证条件的包路径，可以传入多个
     * @return 验证结果，只要有一个条件符合，条件就会成立并返回True
     */
    default boolean verifyClassPackageHasProperties(String classPackage, String... scanPackages) {
        for (String scanPackage : scanPackages) {
            if (Pattern.matches(buildRegexPackage(scanPackage), classPackage)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 转换验证条件，使其支持正则验证
     *
     * @param scanPackage 验证条件包路径
     * @return 验证条件正则
     */
    default String buildRegexPackage(String scanPackage) {
        return scanPackage.replace("**", "[\\w.]*") + ".[\\w]*";
    }
}
