package com.micro.platform.starter.config.loadbalance.ribbon;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.NacosServiceManager;
import com.alibaba.cloud.nacos.ribbon.ExtendBalancer;
import com.alibaba.cloud.nacos.ribbon.NacosServer;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.google.common.collect.ImmutableList;
import com.micro.platform.starter.utils.NumberUtils;
import com.micro.platform.starter.utils.StringUtils;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.DynamicServerListLoadBalancer;
import com.netflix.loadbalancer.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * nacos 扩展 bl rule
 */
@Primary
public class NacosExtendRule extends AbstractLoadBalancerRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(NacosExtendRule.class);
    @Autowired
    private NacosDiscoveryProperties nacosDiscoveryProperties;
    @Autowired
    private NacosServiceManager nacosServiceManager;
    @Override
    public Server choose(Object key) {
        try {
            String clusterName = this.nacosDiscoveryProperties.getClusterName();
            String group = this.nacosDiscoveryProperties.getGroup();
            DynamicServerListLoadBalancer loadBalancer = (DynamicServerListLoadBalancer) getLoadBalancer();
            String name = loadBalancer.getName();
            NamingService namingService = nacosServiceManager.getNamingService(nacosDiscoveryProperties.getNacosProperties());
            List<Instance> instances = namingService.selectInstances(name, group, true);
            if (CollectionUtils.isEmpty(instances)) {
                LOGGER.warn("no instance in service {}", name);
                return null;
            }
            List<Instance> instancesToChoose = instances;
            if (StringUtils.isNotBlank(clusterName)) {
                List<Instance> sameClusterInstances = instances.stream()
                        .filter(instance -> Objects.equals(clusterName, instance.getClusterName()))
                        .collect(Collectors.toList());
                if (!CollectionUtils.isEmpty(sameClusterInstances)) {
                    instancesToChoose = sameClusterInstances;
                }
                else {
                    LOGGER.warn(
                            "A cross-cluster call occurs，name = {}, clusterName = {}, instance = {}",
                            name, clusterName, instances);
                }
            }

            // TODO 多版本扩展(简易版)
            /**
             * 服务器存在客户端版本，则直接调用
             * 服务器不存在客户端版本，则阻止访问
             * 客户端访问版本为NULL，则直接调用最小版本（老项目升级使用）
             */
            List<Instance> targInstances = Collections.emptyList();
            String targVersion = (String) key;
            long targVersionLong = parseVersionLong(targVersion);
            if(StringUtils.isNotBlank(targVersion) && !targVersion.equals("default")){
                targInstances = instancesToChoose.stream().filter(instance -> {
                    String version = instance.getMetadata().get("version");
                    long versionLong = parseVersionLong(version);
                    return targVersionLong == versionLong;
                }).collect(Collectors.toList());
            }else{
                Optional<Instance> reduce = instancesToChoose.stream().reduce((a, b) -> {
                    String aVersion = a.getMetadata().get("version");
                    String bVersion = b.getMetadata().get("version");
                    long aVersionLong = parseVersionLong(aVersion);
                    long bVersionLong = parseVersionLong(bVersion);
                    return aVersionLong > bVersionLong ? b : a;
                });
                targInstances =  reduce.isPresent() ? ImmutableList.of(reduce.get()) : targInstances;
            }

            // nacos依据权重概率分布获取
            Instance instance = ExtendBalancer.getHostByRandomWeight2(targInstances);
            return new NacosServer(instance);
        }
        catch (Exception e) {
            LOGGER.warn("NacosRule error", e);
            return null;
        }
    }

    private long parseVersionLong(String version) {
        String[] split = StringUtils.split(version, ".");
        if(null == split){
            return 0L;
        }
        long versionLong = 1L;
        for (int i = 0; i < split.length; i++) {
            String s = split[i];
            long sLong = NumberUtils.toLong(s, 0L);
            versionLong = versionLong * 1000 + sLong;
        }
        return versionLong;
    }

    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {
    }
}