package com.micro.platform.starter.config;


import com.alibaba.cloud.nacos.ConditionalOnNacosDiscoveryEnabled;
import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.registry.NacosServiceRegistryAutoConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.cloud.client.ConditionalOnDiscoveryEnabled;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * naocs 注册中心增加api元数据
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@ConditionalOnDiscoveryEnabled
@ConditionalOnNacosDiscoveryEnabled
@AutoConfigureBefore(NacosServiceRegistryAutoConfiguration.class)
public class LocalNacosDiscoveryClientConfiguration {
    @Value("${server.servlet.context-path:/}")
    private String contextPath;

    @Value("${spring.application.descriptor:}")
    private String descriptor;

    @Value("${dubbo.registry.version:}")
    private String version;

    @Bean
    public NacosDiscoveryProperties nacosProperties() {
        NacosDiscoveryProperties nacosDiscoveryProperties = new NacosDiscoveryProperties();
        //更改服务详情中的元数据，增加服务注册时间
        nacosDiscoveryProperties.getMetadata().put("startup.time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        nacosDiscoveryProperties.getMetadata().put("contextPath", contextPath);
        nacosDiscoveryProperties.getMetadata().put("descriptor", descriptor);
        nacosDiscoveryProperties.getMetadata().put("version", version);
        return nacosDiscoveryProperties;
    }
}
