package com.micro.platform.starter.config.api;

import com.micro.platform.starter.domain.ApiMateInfo;
import com.micro.platform.starter.utils.ArrayUtils;
import com.micro.platform.starter.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;
import java.util.*;

/**
 * 应用服务 API
 */
@Component
public class ServletApiBuildHandler implements ApiBuildHandler {

    private RequestMappingHandlerMapping mapping;
    private String contextPath;

    public ServletApiBuildHandler(RequestMappingHandlerMapping mapping, String contextPath) {
        this.mapping = mapping;
        if(contextPath == null || "/".equals(contextPath)){
            this.contextPath = "";
        }else{
            this.contextPath = contextPath;
        }
    }

    @Override
    public List<ApiMateInfo> buildApis() {
        List<ApiMateInfo> apis = new ArrayList<>();
        HashMap<String, List<ApiMateInfo>> handlerMapping = new HashMap<>();
        HashMap<String, String> mapClassName2Des = new HashMap<>();
        HashMap<String, String> mapClassName2Path = new HashMap<>();
        // 获取url与类和方法的对应信息
        Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();
        // 遍历服务接口信息，筛选符合条件的数据
        map.forEach((mappingInfo, handlerMethod) -> {
            // 类
            Class<?> controllerClass = handlerMethod.getBeanType();
            // 包路径
            String classPackage = controllerClass.getName();
            if (verifyClassPackageHasProperties(classPackage, "com.micro.**.controller.**")) {
                // 方法
                Method method = handlerMethod.getMethod();
                ApiOperation apiOperation = AnnotatedElementUtils.findMergedAnnotation(method, ApiOperation.class);
                Api api = AnnotatedElementUtils.findMergedAnnotation(controllerClass, Api.class);
                RequestMapping requestMapping = AnnotatedElementUtils.findMergedAnnotation(controllerClass, RequestMapping.class);
                if(apiOperation == null){
                    return;
                }
                // 获取方法路径
                String[] methodPaths = mappingInfo.getPatternsCondition().getPatterns().toArray(new String[]{});
                // 生成数据
                ApiMateInfo apiMateInfo = new ApiMateInfo(classPackage + "#" + method.getName(), this.contextPath + methodPaths[0], apiOperation.value(), null);
                List<ApiMateInfo> apiMateInfos = handlerMapping.get(classPackage);
                if(null == apiMateInfos){
                    apiMateInfos = new ArrayList<>();
                    handlerMapping.put(classPackage, apiMateInfos);
                }
                apiMateInfos.add(apiMateInfo);
                String pdes = api == null ? classPackage : Optional.ofNullable(api.value()).filter(StringUtils::isNotEmpty).orElse(ArrayUtils.get(api.tags(), 0));
                if(requestMapping != null && ArrayUtils.isNotEmpty(requestMapping.value())){
                    mapClassName2Path.put(classPackage, this.contextPath + requestMapping.value()[0]);
                }
                mapClassName2Des.put(classPackage, pdes);
            }
        });
        for (Map.Entry<String, String> stringStringEntry : mapClassName2Des.entrySet()) {
            String key = stringStringEntry.getKey();
            String value = stringStringEntry.getValue();

            List<ApiMateInfo> apiMateInfos = handlerMapping.get(key);
            if(apiMateInfos == null){
                apiMateInfos = Collections.emptyList();
            }
            apiMateInfos.forEach(item -> {
                item.setDes("[" + value + "]--" + item.getDes());
            });
            apis.add(new ApiMateInfo(key, mapClassName2Path.get(key), value, apiMateInfos));
        }
        return apis;
    }
}
