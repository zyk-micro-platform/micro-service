package com.micro.platform.starter.utils;

public class ArrayUtils extends org.apache.commons.lang3.ArrayUtils {
    public static <T> T get(final T[] array, final int index) {
        return get(array, index, null);
    }

    public static <T> T get(final T[] array, final int index, final T defaultValue) {
        return isArrayIndexValid(array, index) ? array[index] : defaultValue;
    }
}
