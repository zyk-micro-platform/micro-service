package com.micro.platform.starter.exception;

import com.micro.platform.starter.basic.BasicErrorCode;
import com.micro.platform.starter.basic.BasicException;

public class BusinessException extends BasicException {
    public BusinessException(BasicErrorCode error) {
        super(error);
    }

    public BusinessException(String message, int errorCode) {
        super(message, errorCode);
    }

    public BusinessException(BasicErrorCode error, Throwable cause) {
        super(error, cause);
    }
    public BusinessException(String message, Throwable cause, int errorCode) {
        super(message, cause, errorCode);
    }
}
