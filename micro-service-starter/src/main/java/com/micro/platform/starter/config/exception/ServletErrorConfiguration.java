package com.micro.platform.starter.config.exception;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration(proxyBeanMethods = false)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@ConditionalOnClass(DefaultErrorAttributes.class)
@Import(GlobalServletExceptionHandle.class)
public class ServletErrorConfiguration extends DefaultErrorAttributes {

    @Bean
    public DefaultErrorAttributes servletErrorAttributes() {
        return new LocalServletErrorAttributes();
    }
}
