package com.micro.platform.starter.utils;

public class NumberUtils extends org.apache.commons.lang3.math.NumberUtils{

    public static Long toLong(Object obj) {
        return toLong(obj, Boolean.TRUE);
    }
    public static Long toLong(Object obj, Boolean hasNull) {
        if(obj == null){
            return hasNull ? null : 0L;
        }
        return toLong(obj.toString());
    }

    public static boolean equalsInteger(Integer a, Integer b) {
        if(a == null || b == null){
            return false;
        }
        return a.equals(b);
    }
}
