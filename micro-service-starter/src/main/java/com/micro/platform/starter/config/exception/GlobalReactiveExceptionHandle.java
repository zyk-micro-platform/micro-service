package com.micro.platform.starter.config.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.DefaultErrorWebExceptionHandler;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;

@Slf4j
public class GlobalReactiveExceptionHandle extends DefaultErrorWebExceptionHandler {
    public GlobalReactiveExceptionHandle(ErrorAttributes errorAttributes, ResourceProperties resourceProperties, ErrorProperties errorProperties, ApplicationContext applicationContext) {
        super(errorAttributes, resourceProperties, errorProperties, applicationContext);
    }

//    @Override
//    public Mono<Void> handle(ServerWebExchange exchange, Throwable e) {
//        Mono<Void> handle = super.handle(exchange, e);
//    }
}
