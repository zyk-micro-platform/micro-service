package com.micro.platform.starter.endpoint;

import com.micro.platform.starter.config.api.ApiBuildHandler;
import com.micro.platform.starter.domain.ApiMateInfo;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;

import java.util.List;

@Endpoint(id = "api")
public class ApiEndpoint {
    private ApiBuildHandler apiBuildHandler;

    public ApiEndpoint(ApiBuildHandler apiBuildHandler) {
        this.apiBuildHandler = apiBuildHandler;
    }

    @ReadOperation
    public List<ApiMateInfo> api() {
        return this.apiBuildHandler.buildApis();
    }
}
