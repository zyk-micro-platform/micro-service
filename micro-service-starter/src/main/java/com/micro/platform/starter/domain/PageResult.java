package com.micro.platform.starter.domain;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.micro.platform.starter.utils.BeanCopyUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

@Data
@ApiModel("分页过滤响应信息")
public class PageResult<T> implements Serializable {
    private static final long serialVersionUID = 8545996863226528798L;

    @ApiModelProperty("总数")
    protected long total = 0;
    @ApiModelProperty("每页显示条数，默认 15")
    protected long size = 15;
    @ApiModelProperty("当前页")
    protected long current = 1;
    @ApiModelProperty("查询数据列表")
    protected List<T> records = Collections.emptyList();

    public PageResult(long total, long size, long current, List<T> records) {
        this.total = total;
        this.size = size;
        this.current = current;
        this.records = records;
    }

    public static <S, T> PageResult<T> build(Page<S> page, Supplier<T> target) {
        List list = BeanCopyUtil.copyListProperties(page.getRecords(), target);
        if(list == null){
            list = Collections.EMPTY_LIST;
        }
        return new PageResult<T>(page.getTotal(), page.getSize(), page.getCurrent(), list);
    }
}
