package com.micro.platform.starter.exception;

import com.micro.platform.starter.enums.CommonErrorCodeEnum;

public class BusinessIllegalArgumentException extends BusinessException {
    public BusinessIllegalArgumentException(String message) {
        super(message, CommonErrorCodeEnum.BASE_PARAM_ERROR.getCode());
    }

    public BusinessIllegalArgumentException(String message, Throwable cause) {
        super(message, cause, CommonErrorCodeEnum.BASE_PARAM_ERROR.getCode());
    }
}
