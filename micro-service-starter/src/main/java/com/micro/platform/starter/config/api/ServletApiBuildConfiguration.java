package com.micro.platform.starter.config.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@Configuration(proxyBeanMethods = false)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@ConditionalOnClass(RequestMappingHandlerMapping.class)
public class ServletApiBuildConfiguration {

    @Value("${server.servlet.context-path:/}")
    private String contextPath;

    @Bean
    @ConditionalOnBean(RequestMappingHandlerMapping.class)
    public ApiBuildHandler apiBuildHandler(RequestMappingHandlerMapping requestMappingHandlerMapping){
        return new ServletApiBuildHandler(requestMappingHandlerMapping, contextPath);
    }
}
