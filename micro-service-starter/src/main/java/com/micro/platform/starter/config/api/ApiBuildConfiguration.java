package com.micro.platform.starter.config.api;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration(proxyBeanMethods = false)
@Import({ReactiveApiBuildConfiguration.class, ServletApiBuildConfiguration.class})
public class ApiBuildConfiguration {
}
