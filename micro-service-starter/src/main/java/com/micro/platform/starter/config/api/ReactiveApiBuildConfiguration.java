package com.micro.platform.starter.config.api;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping;

@Configuration(proxyBeanMethods = false)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
@ConditionalOnClass(RequestMappingHandlerMapping.class)
public class ReactiveApiBuildConfiguration {

    @Bean
    @ConditionalOnBean(RequestMappingHandlerMapping.class)
    public ApiBuildHandler apiBuildHandler(RequestMappingHandlerMapping requestMappingHandlerMapping){
        return new ReactiveApiBuildHandler(requestMappingHandlerMapping);
    }
}
