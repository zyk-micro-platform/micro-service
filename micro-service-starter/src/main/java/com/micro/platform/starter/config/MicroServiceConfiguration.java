package com.micro.platform.starter.config;

import com.micro.platform.starter.component.PasswordEncoderComponent;
import com.micro.platform.starter.component.RedisLimitComponent;
import com.micro.platform.starter.config.endpoint.EndpointConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.core.StringRedisTemplate;

@Configuration
@Import({RestTemplateConfig.class, EndpointConfiguration.class})
public class MicroServiceConfiguration {

    @Bean
    public PasswordEncoderComponent passwordEncoderComponent(){
        return new PasswordEncoderComponent();
    }

    @Bean
    @ConditionalOnBean(StringRedisTemplate.class)
    public RedisLimitComponent redisLimitComponent(StringRedisTemplate stringRedisTemplate){
        return new RedisLimitComponent(stringRedisTemplate);
    }
}
