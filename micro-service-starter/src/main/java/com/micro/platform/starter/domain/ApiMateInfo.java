package com.micro.platform.starter.domain;

import lombok.Data;

import java.util.List;

@Data
public class ApiMateInfo{
    public ApiMateInfo() {
    }

    public ApiMateInfo(String key, String path, String des, List<ApiMateInfo> childrens) {
        this.key = key;
        this.path = path;
        this.des = des;
        this.childrens = childrens;
    }

    private String key;
    private String path;
    private String des;
    private List<ApiMateInfo> childrens;
}