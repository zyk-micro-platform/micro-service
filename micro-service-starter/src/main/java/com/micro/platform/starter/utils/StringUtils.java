package com.micro.platform.starter.utils;

public class StringUtils extends org.apache.commons.lang3.StringUtils {

    public static String toString(Object obj) {
        return toString(obj, Boolean.FALSE);
    }
    public static String toString(Object obj, Boolean hasNull) {
        if(obj == null){
            return hasNull ? null : "";
        }
        return obj.toString();
    }
}
