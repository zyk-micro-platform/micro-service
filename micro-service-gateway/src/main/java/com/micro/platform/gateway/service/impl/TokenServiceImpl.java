package com.micro.platform.gateway.service.impl;

import com.micro.platform.gateway.service.TokenService;
import com.micro.platform.oauth.dto.RefreshTokenInfoDTO;
import com.micro.platform.oauth.dto.TokenInfoDTO;
import com.micro.platform.oauth.rpc.OauthRpcService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceImpl implements TokenService {
    @DubboReference
    private OauthRpcService oauthRpcService;

    @Override
    public TokenInfoDTO analysisAccessToken(String accessToken) {
        TokenInfoDTO tokenInfoDTO = null;
        try{
            tokenInfoDTO = oauthRpcService.analysisAccessToken(accessToken);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return tokenInfoDTO;
    }
}
