package com.micro.platform.gateway.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "local.gateway")
@Data
public class LocalGatewayProperties {
    private LocalApiProperties api = new LocalApiProperties();
}
