package com.micro.platform.gateway.filter;

import com.micro.platform.gateway.constant.GatewayConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;


/**
 * TODO 对OpenApi的支持
 */
@Slf4j
@Component
public class OpenApiFilter implements GlobalFilter, Ordered {

    @Override
    public int getOrder() {
        return Integer.MIN_VALUE + 10;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        if(isOpenApi(exchange)){
            // open-api 打上标识
            exchange.getAttributes().put(GatewayConstants.GATEWAY_FILTER_OPEN_API_FLAG, true);
            chain.filter(exchange);
        }
        return chain.filter(exchange);
    }

    /**
     * 判断是否为OPEN-API
     * @param exchange
     * @return
     */
    private boolean isOpenApi(ServerWebExchange exchange) {
        return false;
    }
}
