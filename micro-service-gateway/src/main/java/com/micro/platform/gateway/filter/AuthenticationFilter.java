package com.micro.platform.gateway.filter;

import com.alibaba.fastjson.JSON;
import com.micro.platform.gateway.constant.GatewayConstants;
import com.micro.platform.gateway.enums.GatewayErrorCodeEnum;
import com.micro.platform.gateway.properties.LocalGatewayProperties;
import com.micro.platform.gateway.service.TokenService;
import com.micro.platform.oauth.dto.TokenInfoDTO;
import com.micro.platform.starter.exception.BusinessException;
import com.micro.platform.starter.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;

@Slf4j
@Component
public class AuthenticationFilter implements GlobalFilter, Ordered {
    @Autowired
    private TokenService tokenService;
    @Autowired
    private LocalGatewayProperties localGatewayProperties;
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String uri = request.getURI().getRawPath();
        log.info("AuthenticationFilter. url: {}", uri);
        // 开放接口
        if (isPublicPath(uri)) {
            return chain.filter(exchange);
        }
        // 获取token
        String accessToken = parseAccessToken(request);
        if (StringUtils.isEmpty(accessToken)) {
            log.error("access-token is emtpy. url: {},headers:{}", request.getURI(), JSON.toJSONString(request.getHeaders()));
            throw new BusinessException(GatewayErrorCodeEnum.TOKEN_INVALID_ERROR);
        }
        // 解析token
        TokenInfoDTO tokenInfo = tokenService.analysisAccessToken(accessToken);
        if(!validTokenInfo(tokenInfo)){
            throw new BusinessException(GatewayErrorCodeEnum.TOKEN_INVALID_ERROR);
        }
        // 密钥放入属性中向下传递
        exchange.getAttributes().put(GatewayConstants.GATEWAY_FILTER_PUBLIC_KEY, tokenInfo.getPublicKey());
        // 内部开放接口
        if (isInnerPublicPath(uri)) {
            return chain.filter(exchange);
        }
        // api校验
        if(!validApi(tokenInfo.getApis(), uri)){
            throw new BusinessException(GatewayErrorCodeEnum.REQUEST_ULTRA_VIRES_ERROR);
        }
        // 设置 userId
        ServerHttpRequest.Builder requestBuilder = request.mutate();
        requestBuilder.header(GatewayConstants.GATEWAY_FILTER_HEADER_USER_ID, StringUtils.toString(tokenInfo.getUserId()));
        return chain.filter(exchange.mutate().request(requestBuilder.build()).build());
    }

    private String parseAccessToken(ServerHttpRequest request) {
        String accessToken = request.getQueryParams().getFirst(GatewayConstants.GATEWAY_FILTER_ACCESS_TOKEN_NAME);
        return StringUtils.isEmpty(accessToken) ? request.getHeaders().getFirst(GatewayConstants.GATEWAY_FILTER_ACCESS_TOKEN_NAME) : accessToken;
    }

    private boolean validApi(List<String> apis, String uri) {
        return apis.contains(uri);
    }

    private boolean validTokenInfo(TokenInfoDTO tokenInfo) {
        if(tokenInfo == null){
            return Boolean.FALSE;
        }
        if(CollectionUtils.isEmpty(tokenInfo.getApis())){
            return Boolean.FALSE;
        }
        if(Objects.isNull(tokenInfo.getUserId())){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public int getOrder() {
        return Integer.MIN_VALUE + 100;
    }

    private boolean isPublicPath(String uri) {
        return localGatewayProperties.getApi().getPublicPath().contains(uri);
    }

    private boolean isInnerPublicPath(String uri) {
        return localGatewayProperties.getApi().getInnerPublicPath().contains(uri);
    }
}
