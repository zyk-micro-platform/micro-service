package com.micro.platform.gateway.constant;

public class GatewayConstants {
    public static final String GATEWAY_FILTER_ACCESS_TOKEN_NAME = "access-token";
    public static final String GATEWAY_FILTER_HEADER_USER_ID = "server-user-id";
    public static final String GATEWAY_FILTER_PUBLIC_KEY = "user-public-key";
    public static final String GATEWAY_FILTER_OPEN_API_FLAG = "open-api-flag";
    public static final String GATEWAY_FILTER_APP_VERSION = "app-version";
}
