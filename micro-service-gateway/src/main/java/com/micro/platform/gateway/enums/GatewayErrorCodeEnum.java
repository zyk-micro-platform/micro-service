package com.micro.platform.gateway.enums;


import com.micro.platform.starter.basic.BasicErrorCode;
import com.micro.platform.starter.enums.ModuleEnum;

public enum GatewayErrorCodeEnum implements BasicErrorCode {
    TOKEN_INVALID_ERROR(101,"token无效，请重新登录"),
    REQUEST_ULTRA_VIRES_ERROR(102,"请求越权"),
    RSA_ID__INVALID_ERROR(103,"RSA-ID 无效"),
    REFRESH_TOKEN_INVALID_ERROR(104,"refresh token无效，请重新登录"),
    ;

    private final int code;

    private final String desc;

    private final int moduleCode = ModuleEnum.OAUTH.getCode();

    GatewayErrorCodeEnum(int code, String desc) {
        this.code = ERROR_CODE_LEVEL * moduleCode + code;
        this.desc = desc;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return desc;
    }

}
