package com.micro.platform.gateway;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("gateway")
@SpringBootApplication
@EnableDiscoveryClient
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("test")
    public String test(){
        ResponseEntity<String> forEntity = restTemplate.getForEntity("http://micro-oauth-server/oauth/token/private-info?access-token=11111111111", String.class);
        System.out.println(JSON.toJSONString(forEntity));
        return forEntity.getBody();
    }
}
