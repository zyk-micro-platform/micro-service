package com.micro.platform.gateway.service;

import com.micro.platform.oauth.dto.RefreshTokenInfoDTO;
import com.micro.platform.oauth.dto.TokenInfoDTO;

public interface TokenService {
    TokenInfoDTO analysisAccessToken(String accessToken);
}
