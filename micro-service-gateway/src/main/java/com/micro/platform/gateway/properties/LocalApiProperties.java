package com.micro.platform.gateway.properties;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class LocalApiProperties {
    private List<String> loginPath = new ArrayList();
    private List<String> publicPath = new ArrayList();
    private List<String> innerPublicPath = new ArrayList();
}
