#!/bin/bash


cd `dirname $0`
BIN_DIR=`pwd`
cd ..
DEPLOY_DIR=`pwd`

SERVER_ID=`sed '/server-id:/!d;s/.*://' bootstrap.yml | tr -d '\r'`
SERVER_PORT=`sed '/port:/!d;s/.*://' bootstrap.yml | tr -d '\r'`
SERVER_JAR_NAME=" -jar $SERVER_ID.jar"


if [ -z "$SERVER_ID" ]; then
    SERVER_ID=`hostname`
fi

PIDS=`ps -ef | grep java | grep -v grep | grep "$SERVER_ID" |awk '{print $2}'`
if [ -n "$PIDS" ]; then
    echo "ERROR: The $SERVER_ID already started!"
    echo "PID: $PIDS"
    exit 1
fi

if [ -n "$SERVER_PORT" ]; then
    SERVER_PORT_COUNT=`netstat -tln | grep $SERVER_PORT | wc -l`
    if [ $SERVER_PORT_COUNT -gt 0 ]; then
        echo "ERROR: The $SERVER_ID port $SERVER_PORT already used!"
        exit 1
    fi
fi

LOGS_DIR=""
if [ -n "$LOGS_FILE" ]; then
    LOGS_DIR=`dirname $LOGS_FILE`
else
    LOGS_DIR=$DEPLOY_DIR/logs
fi
if [ ! -d $LOGS_DIR ]; then
    mkdir $LOGS_DIR
fi
STDOUT_FILE=$LOGS_DIR/start.out

NACOS_OPTS=""
if [ -n "$1" ]; then
  NACOS_OPTS="-DnacosNamespace=$1 "
fi

if [ -n "$2" ]; then
  NACOS_OPTS="$NACOS_OPTS -DnacosAddress=$2 "
else
  NACOS_OPTS="$NACOS_OPTS -DnacosAddress=127.0.0.1:8848 "
fi


JAVA_OPTS=" -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true "

JAVA_MEM_OPTS=" -server -Xms512m -Xmx512m -XX:MetaspaceSize=128m -XX:SurvivorRatio=2 -XX:+UseParallelGC "

if [ ! -f "$STDOUT_FILE" ]; then
  touch "$STDOUT_FILE"
fi

echo -e "Starting the $SERVER_ID ...\c"
nohup java $JAVA_OPTS $NACOS_OPTS $JAVA_MEM_OPTS $SERVER_JAR_NAME > $STDOUT_FILE 2>&1 &

echo "OK!"
PIDS=`ps -ef | grep java | grep -v grep | grep "$DEPLOY_DIR" | awk '{print $2}'`
echo "PID: $PIDS"
echo "STDOUT: $STDOUT_FILE"
